package org.thales.hublabackend.infrastructure.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.product.list.ListProductOutput;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;
import org.thales.hublabackend.domain.product.Product;
import org.thales.hublabackend.domain.user.User;
import org.thales.hublabackend.infrastructure.ControllerTest;
import org.thales.hublabackend.infrastructure.api.service.CreateTransactionsFromFileService;
import org.thales.hublabackend.infrastructure.api.service.ListAllProductsService;
import org.thales.hublabackend.infrastructure.api.service.ListAllTransactionsByProductService;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.thales.hublabackend.shared.utils.Samples.*;

@ControllerTest(controllers = Api.class)
public class ApiTest {

    private static final String ROUTE = "/api/v1";
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private CreateTransactionsFromFileService createTransactionsFromFileService;

    @MockBean
    private ListAllTransactionsByProductService listAllTransactionsByProductService;

    @MockBean
    private ListAllProductsService listAllProductsService;


    @Test
    public void givenValidParams_whenCallsListCategories_shouldReturnCategories() throws Exception {

        final var user = User.with(USER_NAME_SAMPLE, TypeUser.PRODUCER);
        final var product = Product.with(PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, user);
        final var mockedProduct = ProductDto.fromProduct(product);

        final var expectedItems = ListProductOutput.of(List.of(mockedProduct));

        when(listAllProductsService.execute())
                .thenReturn(expectedItems);

        // when
        final var request = get(ROUTE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isCreated())
                .andExpect(jsonPath("$.products", hasSize(1)))
                .andExpect(jsonPath("$.products[0].id", equalTo(mockedProduct.id())))
                .andExpect(jsonPath("$.products[0].name", equalTo(mockedProduct.name())))
                .andExpect(jsonPath("$.products[0].price", equalTo(mockedProduct.price())))
                .andExpect(jsonPath("$.products[0].sells", equalTo(mockedProduct.sells())))
                .andExpect(jsonPath("$.products[0].amountSold", equalTo(mockedProduct.amountSold())))
                .andExpect(jsonPath("$.products[0].producer.id.value", equalTo(user.id())));

        verify(listAllProductsService, times(1)).execute();
    }

}
