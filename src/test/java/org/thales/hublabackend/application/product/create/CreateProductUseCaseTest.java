package org.thales.hublabackend.application.product.create;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.product.find.FindProductByNameInput;
import org.thales.hublabackend.application.product.find.FindProductByNameOutput;
import org.thales.hublabackend.application.product.find.FindProductByNameUseCase;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.application.user.find.FindUserByNameInput;
import org.thales.hublabackend.application.user.find.FindUserByNameOutput;
import org.thales.hublabackend.application.user.find.FindUserByNameUseCase;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.thales.hublabackend.shared.utils.Samples.*;

@ExtendWith(MockitoExtension.class)
public class CreateProductUseCaseTest {

    @Mock
    private IProductGateway<IProductEntity, IProductEntity> productGateway;

    @Mock
    private FindUserByNameUseCase findUserByNameUseCase;

    @Mock
    private FindProductByNameUseCase findProductByNameUseCase;

    @InjectMocks
    private CreateProductUseCase useCase;

    @Test
    void givenValidCommand_whenCallsCreateProduct_shouldReturnProduct() {
        final UserDto userDto = this.buildDefaultUserDto();
        final ProductDto productDto = this.buildDefaultProductDto(userDto);
        FindUserByNameInput input = new FindUserByNameInput(USER_NAME_SAMPLE);

        when(findProductByNameUseCase.execute(any(FindProductByNameInput.class))).thenReturn(FindProductByNameOutput.of(""));

        var output = useCase.execute(new CreateProductInput(PRODUCT_NAME_SAMPLE, userDto, PRODUCT_PRICE_SAMPLE));

        assertNotNull(output.id());

        verify(productGateway, times(1)).save(any(IProductEntity.class));
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.AFFILIATED, 0, null);
    }

    private ProductDto buildDefaultProductDto(UserDto user) {
        return new ProductDto("1", PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, 0, 0, user, null);
    }

}
