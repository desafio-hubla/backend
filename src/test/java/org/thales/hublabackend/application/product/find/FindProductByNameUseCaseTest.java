package org.thales.hublabackend.application.product.find;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.application.user.find.FindUserByNameInput;
import org.thales.hublabackend.application.user.find.FindUserByNameOutput;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.Samples.*;

@ExtendWith(MockitoExtension.class)
public class FindProductByNameUseCaseTest {

    @Mock
    private IProductGateway productGateway;

    @InjectMocks
    private FindProductByNameUseCase findProductByNameUseCase;

    @Test
    void givenValidCommand_whenCallsFindProductByName_shouldReturnProduct() {
        final UserDto userDto = this.buildDefaultUserDto();
        final ProductDto productDto = this.buildDefaultProductDto(userDto);
        FindProductByNameInput input = new FindProductByNameInput(PRODUCT_NAME_SAMPLE);

        when(productGateway.findProductByName(anyString())).thenReturn(Optional.of(productDto));

        FindProductByNameOutput output = findProductByNameUseCase.execute(input);

        Assertions.assertNotNull(output);
        Assertions.assertEquals(PRODUCT_NAME_SAMPLE, output.product().name());
    }

    @Test
    void givenValidParams_whenGatewayThrowException_shouldThrowException() {
        FindProductByNameInput input = new FindProductByNameInput(PRODUCT_NAME_SAMPLE);
        String expected_error_message = "[PRODUCT]: COULD NOT FIND OBJECT {}".replace("{}", input.productName());

        when(productGateway.findProductByName(input.productName())).thenThrow(new IllegalStateException(GATEWAY_FIND_STR));

        FindProductByNameOutput output = findProductByNameUseCase.execute(input);

        Assertions.assertEquals(expected_error_message, output.notification().messages(""));

        verify(productGateway, times(1)).findProductByName(input.productName());
    }

    private ProductDto buildDefaultProductDto(UserDto userDto) {
        return new ProductDto("1", PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, 0, 0, userDto, null);
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.PRODUCER, 0, null);
    }

}
