package org.thales.hublabackend.application.product.list;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_ERROR_STR;
import static org.thales.hublabackend.shared.utils.Samples.*;

@ExtendWith(MockitoExtension.class)
public class ListProductUseCaseTest {

    @Mock
    private IProductGateway productGateway;

    @InjectMocks
    private ListProductUseCase useCase;

    @Test
    public void givenValidQuery_whenCallListProducts_thenShouldReturnListOfProducts() {
        final List<IProductEntity> expectedProducts = List.of(
                this.buildDefaultProductDto(this.buildDefaultUserDto(), "1"),
                this.buildDefaultProductDto(this.buildDefaultUserDto(), "2")
        );

        final Integer expectedItemCount = expectedProducts.size();

        when(productGateway.findAll()).thenReturn(expectedProducts);

        var output = useCase.execute();

        assertEquals(expectedItemCount, output.products().size());
    }

    @Test
    public void givenValidQuery_whenHasNoProducts_thenShouldReturnEmptyListOfProducts() {
        final List<IProductEntity> expected_users = new ArrayList<>();
        final Integer expectedItemCount = 0;

        when(productGateway.findAll()).thenReturn(expected_users);

        var output = useCase.execute();

        assertEquals(expectedItemCount, output.products().size());
    }

    @Test
    public void givenValidQuery_whenGatewayThrowsException_thenShouldThrowException() {
        final Integer expectedItemCount = 1;
        when(productGateway.findAll()).thenThrow(new RuntimeException(GATEWAY_ERROR_STR));

        var output = useCase.execute();

        verify(productGateway, times(1)).findAll();
        assertEquals(expectedItemCount, output.notification().getErrors().size());
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.PRODUCER, 0, null);
    }

    private ProductDto buildDefaultProductDto(UserDto userDto, String productId) {
        return new ProductDto(productId, PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, 0, 0, userDto, null);
    }
}
