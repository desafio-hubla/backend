package org.thales.hublabackend.application.transaction.create;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.enumerator.transaction.TypeTransaction;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import java.time.Instant;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.thales.hublabackend.shared.utils.Samples.*;

@ExtendWith(MockitoExtension.class)
public class CreateTransactionUseCaseTest {

    @Mock
    private ITransactionGateway transactionGateway;

    @InjectMocks
    private CreateTransactionUseCase useCase;

    @Test
    void givenValidCommand_whenCallsCreateTransaction_shouldReturnTransaction() {
        UserDto userDto = buildDefaultUserDto();
        ProductDto productDto = buildDefaultProductDto(userDto);
        final CreateTransactionInput input = buildDefaultTransaction(productDto, userDto, PRODUCT_PRICE_SAMPLE, TypeTransaction.PRODUCER_SALE);

        final CreateTransactionOutput output = useCase.execute(input);

        Assertions.assertNotNull(output);
        Assertions.assertNotNull(output.id());

        verify(transactionGateway, times(1)).save(any(TransactionDto.class));
    }

    @Test
    void givenInvalidNegativePrice_whenCallsCreateTransaction_shouldReturnListOfErrors() {
        final var expectedErrorSize = 1;
        final var expectedErrorMessage = "[TRANSACTION]: YOU CAN NOT HAVE NEGATIVE TRANSACTION";

        UserDto userDto = buildDefaultUserDto();
        ProductDto productDto = buildDefaultProductDto(userDto);
        final CreateTransactionInput input = buildDefaultTransaction(productDto, userDto, -20, TypeTransaction.PRODUCER_SALE);

        final CreateTransactionOutput output = useCase.execute(input);

        Assertions.assertEquals(expectedErrorSize, output.notification().getErrors().size());
        Assertions.assertEquals(expectedErrorMessage, output.notification().messages(""));
        verify(transactionGateway, times(0)).save(any(TransactionDto.class));
    }

    @Test
    void givenInvalidTransactionType_whenCallsCreateTransaction_shouldReturnListOfErrors() {
        final var expectedErrorSize = 1;
        final var expectedErrorMessage = "[TRANSACTION]: THIS KIND OF TRANSACTION DOES NOT EXISTS";

        UserDto userDto = buildDefaultUserDto();
        ProductDto productDto = buildDefaultProductDto(userDto);
        final CreateTransactionInput input = buildDefaultTransaction(productDto, userDto, PRODUCT_PRICE_SAMPLE, null);

        final CreateTransactionOutput output = useCase.execute(input);

        Assertions.assertEquals(expectedErrorSize, output.notification().getErrors().size());
        Assertions.assertEquals(expectedErrorMessage, output.notification().messages(""));
        verify(transactionGateway, times(0)).save(any(TransactionDto.class));
    }

    private ProductDto buildDefaultProductDto(UserDto userDto) {
        return new ProductDto("1", PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, 0, 0, userDto, null);
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.PRODUCER, 0, null);
    }


    private CreateTransactionInput buildDefaultTransaction(ProductDto productDto, UserDto userDto, Integer price, ITypeTransaction transaction) {
        return new CreateTransactionInput(
                transaction,
                Instant.now(),
                price,
                productDto,
                userDto
        );
    }
}
