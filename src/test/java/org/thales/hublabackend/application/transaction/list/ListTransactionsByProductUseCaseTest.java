package org.thales.hublabackend.application.transaction.list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.enumerator.transaction.TypeTransaction;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.Samples.*;

@ExtendWith(MockitoExtension.class)
public class ListTransactionsByProductUseCaseTest {

    @Mock
    private ITransactionGateway transactionGateway;

    @InjectMocks
    private ListTransactionsByProductUseCase listTransactionsByProductUseCase;

    @Test
    void givenValidCommand_whenCallsFindByProduct_shouldReturnTransaction() {
        final Integer expectedItemCount = 1;
        final UserDto userDto = this.buildDefaultUserDto();
        final ProductDto productDto = this.buildDefaultProductDto(userDto);
        final List<TransactionDto> transactions = List.of(this.buildDefaultTransaction(productDto, userDto, PRODUCT_PRICE_SAMPLE, TypeTransaction.PRODUCER_SALE));
        ListTransactionsByProductInput input = new ListTransactionsByProductInput(PRODUCT_NAME_SAMPLE);

        when(transactionGateway.findByProduct(anyString())).thenReturn(transactions);

        ListTransactionsByProductOutput output = listTransactionsByProductUseCase.execute(input);

        Assertions.assertNotNull(output);
        assertEquals(expectedItemCount, output.transactions().size());
    }

    @Test
    void givenValidParams_whenGatewayThrowException_shouldThrowException() {
        final var expectedErrorCount = 1;
        ListTransactionsByProductInput input = new ListTransactionsByProductInput("1");
        String expectedErrorMessage = "[TRANSACTION]: GATEWAY ERROR.";

        when(transactionGateway.findByProduct(input.productId())).thenThrow(new IllegalStateException(GATEWAY_FIND_STR));

        ListTransactionsByProductOutput output = listTransactionsByProductUseCase.execute(input);

        Assertions.assertEquals(expectedErrorMessage, output.notification().messages(""));
        Assertions.assertEquals(expectedErrorCount, output.notification().getErrors().size());

        verify(transactionGateway, times(1)).findByProduct("1");
    }

    private ProductDto buildDefaultProductDto(UserDto userDto) {
        return new ProductDto("1", PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, 0, 0, userDto, null);
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.PRODUCER, 0, null);
    }


    private TransactionDto buildDefaultTransaction(ProductDto productDto, UserDto userDto, Integer price, ITypeTransaction transaction) {
        return new TransactionDto(
                "1",
                transaction,
                Instant.now(),
                price,
                productDto,
                userDto,
                null
        );
    }

}
