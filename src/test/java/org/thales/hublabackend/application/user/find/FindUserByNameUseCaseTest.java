package org.thales.hublabackend.application.user.find;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.user.IUserGateway;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.Samples.USER_NAME_SAMPLE;

@ExtendWith(MockitoExtension.class)
public class FindUserByNameUseCaseTest {

    @Mock
    private IUserGateway userGateway;

    @InjectMocks
    private FindUserByNameUseCase findUserByNameUseCase;

    @Test
    void givenValidCommand_whenCallsFindByName_shouldReturnUser() {
        final UserDto userDto = this.buildDefaultUserDto();
        FindUserByNameInput input = new FindUserByNameInput(USER_NAME_SAMPLE);

        when(userGateway.findUserByName(anyString())).thenReturn(Optional.of(userDto));

        FindUserByNameOutput output = findUserByNameUseCase.execute(input);

        Assertions.assertNotNull(output);
        Assertions.assertEquals(USER_NAME_SAMPLE, output.user().name());
    }

    @Test
    void givenValidParams_whenGatewayThrowException_shouldThrowException() {
        FindUserByNameInput input = new FindUserByNameInput(USER_NAME_SAMPLE);
        String expected_error_message = "[USER]: COULD NOT FIND OBJECT {}".replace("{}", input.userName());

        when(userGateway.findUserByName(input.userName())).thenThrow(new IllegalStateException(GATEWAY_FIND_STR));

        FindUserByNameOutput output = findUserByNameUseCase.execute(input);

        Assertions.assertEquals(expected_error_message, output.notification().messages(""));

        verify(userGateway, times(1)).findUserByName(input.userName());
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.AFFILIATED, 0, null);
    }

}
