package org.thales.hublabackend.application.user.create;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thales.hublabackend.application.user.IUserGateway;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.application.user.find.FindUserByNameInput;
import org.thales.hublabackend.application.user.find.FindUserByNameOutput;
import org.thales.hublabackend.application.user.find.FindUserByNameUseCase;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_ERROR_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.USER_ALREADY_EXISTS;
import static org.thales.hublabackend.shared.utils.Samples.USER_NAME_SAMPLE;

@ExtendWith(MockitoExtension.class)
public class CreateUserUseCaseTest {

    @Mock
    private IUserGateway userGateway;

    @Mock
    private FindUserByNameUseCase findUserByNameUseCase;

    @InjectMocks
    private CreateUserUseCase useCase;

    @Test
    void givenValidCommand_whenCallsCreateUser_shouldReturnUser() {
        final var input = new CreateUserInput(USER_NAME_SAMPLE, TypeUser.PRODUCER);

        FindUserByNameOutput findUserByNameOutput = FindUserByNameOutput.of("");
        when(findUserByNameUseCase.execute(new FindUserByNameInput(USER_NAME_SAMPLE))).thenReturn(findUserByNameOutput);

        when(userGateway.save(any())).thenAnswer(returnsFirstArg());

            final CreateUserOutput output = useCase.execute(input);

        Assertions.assertNotNull(output);
        Assertions.assertNotNull(output.id());

        verify(userGateway, times(1)).save(any(UserDto.class));
    }

    @Test
    void givenValidCommandAndExistentUser_whenCallsCreateUser_shouldReturnListOfErrors() {
        final var input = new CreateUserInput(USER_NAME_SAMPLE, TypeUser.PRODUCER);
        final var expectedMessage = "[USER]: "+USER_ALREADY_EXISTS.replace("{}", USER_NAME_SAMPLE);

        UserDto userDto = this.buildDefaultUserDto();
        FindUserByNameOutput findUserByNameOutput = FindUserByNameOutput.of(userDto);

        when(findUserByNameUseCase.execute(new FindUserByNameInput(USER_NAME_SAMPLE))).thenReturn(findUserByNameOutput);

        final var createUser = useCase.execute(input);

        Assertions.assertNotNull(createUser.notificationErrors());
        Assertions.assertEquals(createUser.notificationErrors().messages(""), expectedMessage);
    }

    @Test
    void givenInvalidBlankName_whenCallsCreateUser_shouldReturnListOfErrors() {
        final String expected_error_message = "[USER]: 'NAME' SHOULD NOT BE BLANK.[USER]: 'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";

        final var command = new CreateUserInput("", TypeUser.AFFILIATED);
        final var output = useCase.execute(command);

        Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));
        verify(userGateway, times(0)).save(any(UserDto.class));
    }

    @Test
    void givenValidCommand_whenGatewayThrowsAnError_shouldReturnListOfErrors() {

        final var command = new CreateUserInput(USER_NAME_SAMPLE, TypeUser.AFFILIATED);

        FindUserByNameOutput findUserByNameOutput = FindUserByNameOutput.of("");
        when(findUserByNameUseCase.execute(new FindUserByNameInput(USER_NAME_SAMPLE))).thenReturn(findUserByNameOutput);

        when(userGateway.save(any())).thenThrow(new RuntimeException(GATEWAY_ERROR_STR));

        final var output = useCase.execute(command);

        Assertions.assertNotNull(output.notificationErrors());
        verify(userGateway, times(1)).save(any(UserDto.class));
    }

    private UserDto buildDefaultUserDto() {
        return new UserDto("1", USER_NAME_SAMPLE, TypeUser.AFFILIATED, 0, null);
    }

}
