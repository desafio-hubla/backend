package org.thales.hublabackend.domain.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import static org.thales.hublabackend.shared.utils.Constants.NAME_STR;
import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;
import static org.thales.hublabackend.shared.utils.Samples.USER_NAME_SAMPLE;

class UserTest {

    @Test
    void givenValidParams_whenCallNewUser_thenShouldCreateUser() {
        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        createdUser.validate();

        Assertions.assertFalse(createdUser.notification().hasErrors());
        Assertions.assertNotNull(createdUser);
        Assertions.assertNotNull(createdUser.getId());
        Assertions.assertEquals(USER_NAME_SAMPLE, createdUser.name());
        Assertions.assertEquals(0, createdUser.balance());
    }

    @Test
    void givenInValidParamUndefinedKindOfUser_whenCallNewUser_thenShouldReturnListOfErrors() {
        final Integer expected_error_count = 1;
        final String expected_error_message = THIS_KIND_OF_USER_DOES_NOT_EXISTS.replace("{}", TRANSACTION_STR);

        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(3));
        createdUser.validate();

        Assertions.assertEquals(expected_error_count, createdUser.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, createdUser.notification().getErrors().get(0).message());
    }

    @Test
    void givenInvalidNullName_whenCallNewUser_thenReturnListOfErrors() {
        final Integer expected_error_count = 1;
        final String expected_error_message = STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR);

        User user = User.with(null, TypeUser.getByTypeOrNull(1));
        user.validate();

        Assertions.assertEquals(expected_error_count, user.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, user.notification().getErrors().get(0).message());
    }

    @Test
    void givenInvalidBlankName_whenCallNewUser_thenReturnListOfErrors() {
        final Integer expected_error_count = 2;

        User user = User.with("", TypeUser.getByTypeOrNull(1));
        user.validate();

        Assertions.assertEquals(expected_error_count, user.notification().getErrors().size());
    }

    @Test
    void givenInvalidNameLengthLesserThan3_whenCallNewUser_thenReturnListOfErrors() {
        final String expected_name = "Fi ";

        final String expected_error_message = NAME_LENGTH_INVALID;
        final Integer expected_error_count = 1;

        User user = User.with(expected_name, TypeUser.getByTypeOrNull(1));
        user.validate();

        Assertions.assertEquals(expected_error_count, user.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, user.notification().getErrors().get(0).message());
    }

}
