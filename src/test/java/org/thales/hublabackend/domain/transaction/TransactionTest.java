package org.thales.hublabackend.domain.transaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.thales.hublabackend.domain.enumerator.transaction.TypeTransaction;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;
import org.thales.hublabackend.domain.product.Product;
import org.thales.hublabackend.domain.user.User;

import java.time.Instant;

import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;
import static org.thales.hublabackend.shared.utils.Samples.*;

public class TransactionTest {

    @Test
    void givenValidParams_whenCallNewTransaction_thenShouldCreateTransaction() {
        final var expectedDate = Instant.now();
        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, createdUser);
        final Transaction createdTransaction = Transaction.with(TypeTransaction.getByTypeOrNull(1), expectedDate, PRODUCT_PRICE_SAMPLE, createdProduct, createdUser);
        createdTransaction.validate();

        Assertions.assertFalse(createdTransaction.notification().hasErrors());
        Assertions.assertNotNull(createdTransaction);
        Assertions.assertNotNull(createdTransaction.getId());
        Assertions.assertEquals(TypeTransaction.PRODUCER_SALE, createdTransaction.kind());
        Assertions.assertEquals(expectedDate, createdTransaction.createdAt());
        Assertions.assertEquals(PRODUCT_PRICE_SAMPLE, createdTransaction.total());
        Assertions.assertEquals(createdUser.id(), createdTransaction.seller().id());
        Assertions.assertEquals(createdProduct.id(), createdTransaction.product().id());
    }

    @Test
    void givenInValidParamUndefinedKindOfTransaction_whenCallNewTransaction_thenShouldReturnListOfErrors() {
        final Integer expected_error_count = 1;
        final String expected_error_message = THIS_KIND_OF_TRANSACTION_DOES_NOT_EXISTS.replace("{}", TRANSACTION_STR);

        final var expectedDate = Instant.now();
        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, createdUser);
        final Transaction createdTransaction = Transaction.with(TypeTransaction.getByTypeOrNull(5), expectedDate, PRODUCT_PRICE_SAMPLE, createdProduct, createdUser);
        createdTransaction.validate();

        Assertions.assertEquals(expected_error_count, createdTransaction.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, createdTransaction.notification().getErrors().get(0).message());
    }

    @Test
    void givenInvalidParamTotalNegative_whenCallNewTransaction_thenReturnListOfErrors() {

        final String expected_error_message = NEGATIVE_TRANSACTION;
        final Integer expected_error_count = 1;

        final var expectedDate = Instant.now();
        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, createdUser);
        final Transaction createdTransaction = Transaction.with(TypeTransaction.getByTypeOrNull(1), expectedDate, -20, createdProduct, createdUser);
        createdTransaction.validate();

        Assertions.assertEquals(expected_error_count, createdTransaction.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, createdTransaction.notification().getErrors().get(0).message());
    }

}
