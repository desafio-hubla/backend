package org.thales.hublabackend.domain.product;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;
import org.thales.hublabackend.domain.user.User;

import static org.thales.hublabackend.shared.utils.Constants.NAME_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;
import static org.thales.hublabackend.shared.utils.Samples.*;

public class ProductTest {

    @Test
    void givenValidParams_whenCallNewProduct_thenShouldCreateProduct() {
        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, createdUser);
        createdProduct.validate();

        Assertions.assertFalse(createdProduct.notification().hasErrors());
        Assertions.assertNotNull(createdProduct);
        Assertions.assertNotNull(createdProduct.getId());
        Assertions.assertEquals(PRODUCT_NAME_SAMPLE, createdProduct.name());
        Assertions.assertEquals(PRODUCT_PRICE_SAMPLE, createdProduct.price());
        Assertions.assertEquals(0, createdProduct.sells());
        Assertions.assertEquals(0, createdProduct.amountSold());
        Assertions.assertEquals(createdUser.id(), createdProduct.producer().id());
    }

    @Test
    void givenValidProduct_whenCallProductSold_thenShouldIncreaseSellsAndAmountSold() {
        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(PRODUCT_NAME_SAMPLE, PRODUCT_PRICE_SAMPLE, createdUser);
        createdProduct.validate();

        createdProduct.sold();

        Assertions.assertEquals(createdProduct.sells(), PRODUCT_PRICE_SAMPLE);
        Assertions.assertEquals(createdProduct.amountSold(), 1);
    }

    @Test
    void givenInvalidNullName_whenCallNewProduct_thenReturnListOfErrors() {
        final Integer expected_error_count = 1;
        final String expected_error_message = STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR);

        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(null, PRODUCT_PRICE_SAMPLE, createdUser);
        createdProduct.validate();

        Assertions.assertEquals(expected_error_count, createdProduct.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, createdProduct.notification().getErrors().get(0).message());
    }

    @Test
    void givenInvalidBlankName_whenCallNewProduct_thenReturnListOfErrors() {
        final Integer expected_error_count = 2;

        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with("", PRODUCT_PRICE_SAMPLE, createdUser);
        createdProduct.validate();

        Assertions.assertEquals(expected_error_count, createdProduct.notification().getErrors().size());
    }

    @Test
    void givenInvalidNameLengthLesserThan3_whenCallNewProduct_thenReturnListOfErrors() {
        final String expected_name = "Fi ";

        final String expected_error_message = NAME_LENGTH_INVALID;
        final Integer expected_error_count = 1;

        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(expected_name, PRODUCT_PRICE_SAMPLE, createdUser);
        createdProduct.validate();

        Assertions.assertEquals(expected_error_count, createdProduct.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, createdProduct.notification().getErrors().get(0).message());
    }

    @Test
    void givenInvalidPrice_whenCallNewProduct_thenReturnListOfErrors() {

        final String expected_error_message = NEGATIVE_PRICE_OF_PRODUCT;
        final Integer expected_error_count = 1;

        final User createdUser = User.with(USER_NAME_SAMPLE, TypeUser.getByTypeOrNull(1));
        final Product createdProduct = Product.with(PRODUCT_NAME_SAMPLE, -20, createdUser);
        createdProduct.validate();

        Assertions.assertEquals(expected_error_count, createdProduct.notification().getErrors().size());
        Assertions.assertEquals(expected_error_message, createdProduct.notification().getErrors().get(0).message());
    }

}
