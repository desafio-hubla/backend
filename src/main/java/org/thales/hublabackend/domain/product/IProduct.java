package org.thales.hublabackend.domain.product;

import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.INotification;

public interface IProduct {

    public String id();
    public String name();
    public INotification notification();
    public Integer price();
    public Integer sells();
    public IUser producer();
    public Integer amountSold();
    public void sold();
}
