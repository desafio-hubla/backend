package org.thales.hublabackend.domain.product;

interface IProductValidation {

    public abstract void validate(IProduct product);

}
