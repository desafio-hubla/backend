package org.thales.hublabackend.domain.product;

import org.thales.hublabackend.domain.Aggregate;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.Notification;

public class Product extends Aggregate<ProductId> implements IProduct {

    private String name;
    private Integer price;
    private Integer sells;
    private Integer amountSold;
    private IUser producer;

    private Product(String name, Integer price, IUser user) {
        super(ProductId.unique(), new Notification());
        this.name = name;
        this.price = price;
        this.producer = user;
        this.sells = 0;
        this.amountSold = 0;
    }

    private Product(String id, String name, Integer price, IUser user, Integer sells, Integer amountSold) {
        super(ProductId.from(id), new Notification());
        this.name = name;
        this.price = price;
        this.producer = user;
        this.sells = sells;
        this.amountSold = amountSold;
    }

    public static Product with(String name, Integer price, IUser user) {
        return new Product(name, price, user);
    }

    public static Product with(String id, String name, Integer price, IUser user, Integer sells, Integer amountSold) {
        return new Product(id, name, price, user, sells, amountSold);
    }

    @Override
    public void validate() {
        new ProductValidation().validate(this);
    }

    @Override
    public String id() {
        return this.getId().getValue();
    }

    @Override
    public String name() {
        return this.name;
    }

    public Integer price() {
        return price;
    }

    public Integer sells() {
        return sells;
    }

    public IUser producer() {
        return producer;
    }

    public Integer amountSold() {
        return amountSold;
    }

    @Override
    public void sold() {
        this.sells += this.price;
        this.amountSold += 1;
    }
}
