package org.thales.hublabackend.domain.product;

import static org.thales.hublabackend.shared.utils.Constants.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;

class ProductValidation implements IProductValidation {

    @Override
    public void validate(IProduct product) {
        this.checkNameConstraints(product);
        this.checkAmountOfSells(product);
        this.checkPrice(product);
    }

    private void checkPrice(IProduct product) {
        if(product.price() < 0)
            product.notification().append(NEGATIVE_PRICE_OF_PRODUCT, PRODUCT_STR);
    }

    private void checkAmountOfSells(IProduct product) {
        if(product.sells() < 0)
            product.notification().append(NEGATIVE_AMOUNT_OF_SELLS, PRODUCT_STR);
    }

    private void checkNameConstraints(IProduct product) {
        if (isNameNullOrBlank(product)) return;
        isValidNameLength(product);
    }

    private void isValidNameLength(IProduct product) {
        final String name = product.name().trim();
        if ((name.length() < MIN_NAME_LEN) || (name.length() > MAX_NAME_LEN)) {
            product.notification().append(NAME_LENGTH_INVALID.replace("{}", NAME_STR), PRODUCT_STR);
        }
    }

    private boolean isNameNullOrBlank(IProduct product) {
        if (product.name() == null) {
            product.notification().append(STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR), PRODUCT_STR);
            return true;
        } else if (product.name().isEmpty()) {
            product.notification().append(STRING_SHOULD_NOT_BE_BLANK.replace("{}", NAME_STR), PRODUCT_STR);
        }
        return false;
    }

}
