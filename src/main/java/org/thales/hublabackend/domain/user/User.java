package org.thales.hublabackend.domain.user;

import org.thales.hublabackend.domain.Aggregate;
import org.thales.hublabackend.domain.enumerator.user.ITypeUser;
import org.thales.hublabackend.shared.notification.Notification;

public class User extends Aggregate<UserId> implements IUser {

    private String name;
    private Integer balance;
    private ITypeUser type;

    private User(String name, ITypeUser type) {
        super(UserId.unique(), new Notification());
        this.name = name;
        this.type = type;
        this.balance = 0;
    }

    private User(String id, String name, ITypeUser type, Integer balance) {
        super(UserId.from(id), new Notification());
        this.name = name;
        this.type = type;
        this.balance = balance;
    }

    public static User with(String id, String name, ITypeUser kind, Integer balance) {
        return new User(id, name, kind, balance);
    }

    public static User with(String name, ITypeUser kind) {
        return new User(name, kind);
    }

    @Override
    public void validate() {
        new UserValidator().validate(this);
    }

    public void receiveComission(Integer comission) {
        this.balance += comission;
    }

    @Override
    public String id() {
        return this.id.getValue();
    }

    public String name() {
        return name;
    }

    public Integer balance() {
        return balance;
    }

    public ITypeUser type() {
        return type;
    }
}
