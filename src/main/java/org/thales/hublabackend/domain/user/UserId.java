package org.thales.hublabackend.domain.user;

import org.thales.hublabackend.domain.Identifier;

import java.util.Objects;
import java.util.UUID;

class UserId extends Identifier {

    private final String value;

    UserId(String value) {
        Objects.requireNonNull(value);
        this.value = value;
    }

    public static UserId unique() {
        return UserId.from(UUID.randomUUID());
    }

    public static UserId from(String id) {
        return new UserId(id);
    }

    public static UserId from(UUID id) {
        return new UserId(id.toString().toLowerCase());
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserId userId = (UserId) o;
        return getValue().equals(userId.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }

}
