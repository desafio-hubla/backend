package org.thales.hublabackend.domain.user;

import org.thales.hublabackend.domain.enumerator.user.ITypeUser;
import org.thales.hublabackend.shared.notification.INotification;

public interface IUser {

    public void receiveComission(Integer comission);
    public String id();
    public String name();
    public Integer balance();
    public INotification notification();
    public ITypeUser type();

}
