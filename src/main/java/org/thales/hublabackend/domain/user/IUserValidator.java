package org.thales.hublabackend.domain.user;

interface IUserValidator {

    public abstract void validate(IUser user);

}
