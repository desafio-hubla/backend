package org.thales.hublabackend.domain.user;

import static org.thales.hublabackend.shared.utils.Constants.*;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;

class UserValidator implements IUserValidator {

    @Override
    public void validate(IUser user) {
        this.checkNameConstraints(user);
        this.checkKind(user);
    }

    private void checkKind(IUser user) {
        if(user.type() == null)
            user.notification().append(THIS_KIND_OF_USER_DOES_NOT_EXISTS, TRANSACTION_STR);
    }

    private void checkNameConstraints(IUser user) {
        if (isNameNullOrBlank(user)) return;
        isValidNameLength(user);
    }

    private static void isValidNameLength(IUser user) {
        final String name = user.name().trim();
        if ((name.length() < MIN_NAME_LEN) || (name.length() > MAX_NAME_LEN)) {
            user.notification().append(NAME_LENGTH_INVALID.replace("{}", NAME_STR), USER_STR);
        }
    }

    private static boolean isNameNullOrBlank(IUser user) {
        if (user.name() == null) {
            user.notification().append(STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR), USER_STR);
            return true;
        } else if (user.name().isEmpty()) {
            user.notification().append(STRING_SHOULD_NOT_BE_BLANK.replace("{}", NAME_STR), USER_STR);
        }
        return false;
    }

}
