package org.thales.hublabackend.domain.enumerator.transaction;

import java.util.List;

public enum TypeTransaction implements ITypeTransaction {
    PRODUCER_SALE(1, "Venda produtor", "Entrada", "+"),
    AFFILIATE_SALE(2, "Venda afiliado", "Entrada", "+"),
    PAID_COMMISSION(3, "Comissão paga", "Saída", "-"),
    RECEIVED_COMMISSION(4, "Comissão recebida", "Entrada", "+");

    private final int type;
    private final String description;
    private final String nature;
    private final String signal;

    TypeTransaction(int type, String description, String nature, String signal) {
        this.type = type;
        this.description = description;
        this.nature = nature;
        this.signal = signal;
    }


    public static TypeTransaction getByTypeOrNull(int type) {
        final var list = List.of(TypeTransaction.values()).stream().filter(transactionType -> transactionType.type == type).toList();

        if(list.size() == 0)
            return null;

        return list.get(0);
    }

    public static Boolean isSale(ITypeTransaction type) {
        return type.equals(PRODUCER_SALE) || type.equals(AFFILIATE_SALE);
    }

    public static Boolean isCommission(ITypeTransaction type) {
        return type.equals(PAID_COMMISSION) || type.equals(RECEIVED_COMMISSION);
    }

    public int getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getNature() {
        return nature;
    }

    public String getSignal() {
        return signal;
    }
}
