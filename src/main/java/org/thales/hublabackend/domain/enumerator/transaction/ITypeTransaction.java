package org.thales.hublabackend.domain.enumerator.transaction;

public interface ITypeTransaction {

    public int getType();

}
