package org.thales.hublabackend.domain.enumerator.user;

public interface ITypeUser {

    public Integer get();

}
