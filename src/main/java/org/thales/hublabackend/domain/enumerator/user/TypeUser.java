package org.thales.hublabackend.domain.enumerator.user;

import java.util.List;

public enum TypeUser implements ITypeUser {

    PRODUCER(0),
    AFFILIATED(1);

    private final Integer userType;

    TypeUser(Integer userType) {
        this.userType = userType;
    }

    public Integer get() {
        return userType;
    }

    public static TypeUser getByTypeOrNull(int type) {
        final var list = List.of(TypeUser.values()).stream().filter(userType -> userType.userType == type).toList();

        if(list.size() == 0)
            return null;

        return list.get(0);
    }


}
