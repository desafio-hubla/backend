package org.thales.hublabackend.domain;


import org.thales.hublabackend.shared.notification.INotification;

public abstract class Aggregate<ID extends Identifier> extends Entity<ID> {

  private final INotification notification;

  protected Aggregate(ID id, INotification notification) {
    super(id);
    this.notification = notification;
  }

  public INotification notification() {
    return notification;
  }

  public abstract void validate();
}
