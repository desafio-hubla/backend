package org.thales.hublabackend.domain.transaction;

import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.INotification;

import java.time.Instant;

public interface ITransaction {

    public String id();
    public ITypeTransaction kind();
    public Instant createdAt();
    public Integer total();
    public IProduct product();
    public INotification notification();
    public IUser seller();
    public void changeBalance();
}
