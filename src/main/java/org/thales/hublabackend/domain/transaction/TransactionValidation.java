package org.thales.hublabackend.domain.transaction;

import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;

class TransactionValidation implements ITransactionValidation {

    @Override
    public void validate(ITransaction transaction) {
        this.checkPrice(transaction);
        this.checkKind(transaction);
    }

    private void checkKind(ITransaction transaction) {
        if(transaction.kind() == null)
            transaction.notification().append(THIS_KIND_OF_TRANSACTION_DOES_NOT_EXISTS, TRANSACTION_STR);
    }

    private void checkPrice(ITransaction transaction) {
        if(transaction.total() < 0)
            transaction.notification().append(NEGATIVE_TRANSACTION, TRANSACTION_STR);
    }

}
