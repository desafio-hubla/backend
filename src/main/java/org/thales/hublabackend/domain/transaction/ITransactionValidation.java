package org.thales.hublabackend.domain.transaction;

interface ITransactionValidation {

    public abstract void validate(ITransaction transaction);

}
