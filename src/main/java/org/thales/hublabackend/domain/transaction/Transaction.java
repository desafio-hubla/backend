package org.thales.hublabackend.domain.transaction;

import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.domain.Aggregate;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.enumerator.transaction.TypeTransaction;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.Notification;

import java.time.Instant;

public class Transaction extends Aggregate<TransactionId> implements ITransaction {

    private ITypeTransaction kind;
    private Instant createdAt;
    private Integer total;
    private IProduct product;
    private IUser seller;

    protected Transaction(ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller) {
        super(TransactionId.unique(), new Notification());
        this.kind = kind;
        this.createdAt = createdAt;
        this.total = total;
        this.product = product;
        this.seller = seller;
    }

    protected Transaction(TransactionId id, ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller) {
        super(id, new Notification());
        this.kind = kind;
        this.createdAt = createdAt;
        this.total = total;
        this.product = product;
        this.seller = seller;
    }

    public static Transaction with(ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller) {
        return new Transaction(kind, createdAt, total, product, seller);
    }

    public static Transaction of(String id, ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller) {
        return new Transaction(TransactionId.from(id), kind, createdAt, total, product, seller);
    }

    @Override
    public void validate() {
        new TransactionValidation().validate(this);
    }

    public void changeBalance() {
        this.changeSellerBalance();
        this.changeProducerBalance();
        this.changeProductSells();
    }

    private void changeProductSells() {
        if(TypeTransaction.isSale(this.kind))
            this.product().sold();
    }

    private void changeProducerBalance() {
        String seller = this.seller.name();
        String producer = this.product.producer().name();
        if(!seller.equals(producer))
            this.product.producer().receiveComission(this.product.price() - this.total);
    }

    private void changeSellerBalance() {
        if(TypeTransaction.isCommission(this.kind))
            this.seller.receiveComission(this.total);
    }

    @Override
    public String id() {
        return this.id.getValue();
    }

    public ITypeTransaction kind() {
        return kind;
    }

    public Instant createdAt() {
        return createdAt;
    }

    public Integer total() {
        return total;
    }

    public IProduct product() {
        return product;
    }

    public IUser seller() {
        return seller;
    }
}
