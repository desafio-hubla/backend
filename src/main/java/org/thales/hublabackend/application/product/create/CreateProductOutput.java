package org.thales.hublabackend.application.product.create;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;

import static org.thales.hublabackend.shared.utils.Constants.PRODUCT_STR;

public record CreateProductOutput(String id, String name, Integer price, Integer sells, Integer amountSold, IUser producer, INotification notification) {

    public static CreateProductOutput of(IProductEntity product) {
        return new CreateProductOutput(product.id(), product.name(), product.price(), product.sells(), product.amountSold(), product.producer(), null);
    }

    public static CreateProductOutput of(INotification notification) {
        return new CreateProductOutput(null, null, null, null, null, null, notification);
    }

    public static CreateProductOutput of(String message) {
        INotification notification = new Notification();
        notification.append(message, PRODUCT_STR);
        return new CreateProductOutput(null, null, null, null, null, null, notification);
    }

}
