package org.thales.hublabackend.application.product.find;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.Objects;
import java.util.Optional;

import static org.thales.hublabackend.shared.utils.ActionConstants.FIND_PRODUCT_BY_NAME;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.PRODUCT_NOT_FOUND;
import static org.thales.hublabackend.shared.utils.SuccessConstants.PRODUCT_FOUND;

public class FindProductByNameUseCase {

    private static final ILog log = new Log(FindProductByNameUseCase.class);

    private final IProductGateway productGateway;

    private FindProductByNameUseCase(IProductGateway productGateway) {
        this.productGateway = Objects.requireNonNull(productGateway);
    }

    public static FindProductByNameUseCase of(IProductGateway gateway) {
        return new FindProductByNameUseCase(gateway);
    }

    public FindProductByNameOutput execute(FindProductByNameInput input) {
        var name = input.productName();
        log.info(FIND_PRODUCT_BY_NAME, name);
        Optional<IProductEntity> product = Optional.empty();
        try {
            product = this.productGateway.findProductByName(name);
        } catch (Exception e) {
            log.error(e.getMessage());
            return FindProductByNameOutput.of(GATEWAY_FIND_STR.replace("{}", name));
        }


        if (product.isEmpty()) {
            String message = PRODUCT_NOT_FOUND.replace("{}", name);
            log.info(message);
            return FindProductByNameOutput.of(message);
        }

        log.info(PRODUCT_FOUND, product.get().toString());
        return FindProductByNameOutput.of(product.get());
    }

}
