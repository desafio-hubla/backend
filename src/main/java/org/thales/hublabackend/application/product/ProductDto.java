package org.thales.hublabackend.application.product;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.product.Product;
import org.thales.hublabackend.domain.transaction.Transaction;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.INotification;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ProductDto implements IProductEntity {

    private String id;
    private String name;
    private Integer price;
    private Integer sells;
    private Integer amountSold;
    private IUser producer;
    private INotification notification;

    public ProductDto(String id, String name, Integer price, Integer sells, Integer amountSold, IUser producer, INotification notification) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.sells = sells;
        this.amountSold = amountSold;
        this.producer = producer;
        this.notification = notification;
    }

    public static ProductDto fromProduct(IProduct product) {
        return new ProductDto(product.id(), product.name(), product.price(), product.sells(), product.amountSold(), product.producer(), product.notification());
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public INotification notification() {
        return notification;
    }

    @Override
    public Integer price() {
        return price;
    }

    @Override
    public Integer sells() {
        return sells;
    }

    @Override
    public IUser producer() {
        return producer;
    }

    @Override
    public Integer amountSold() {
        return amountSold;
    }

    @Override
    public void sold() {
        Product product = Product.with(id, name, price, producer, sells, amountSold);
        product.sold();

        this.sells = product.sells();
        this.amountSold = product.amountSold();
    }


}
