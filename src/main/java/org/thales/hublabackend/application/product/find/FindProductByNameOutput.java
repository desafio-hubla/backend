package org.thales.hublabackend.application.product.find;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;
import org.thales.hublabackend.shared.notification.NotificationErrorProps;

import static org.thales.hublabackend.shared.utils.Constants.PRODUCT_STR;

public record FindProductByNameOutput(IProductEntity product, INotification notification) {

    public static FindProductByNameOutput of(String message) {
        INotification notification = new Notification();
        notification.append(new NotificationErrorProps(message, PRODUCT_STR));
        return new FindProductByNameOutput(null, notification);
    }

    public static FindProductByNameOutput of(IProductEntity product) {
        return new FindProductByNameOutput(product, null);
    }

}
