package org.thales.hublabackend.application.product.list;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;
import org.thales.hublabackend.shared.notification.NotificationErrorProps;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.thales.hublabackend.shared.utils.Constants.PRODUCT_STR;

public record ListProductOutput(List<IProductEntity> products, INotification notification) {

    public static ListProductOutput of(String message) {
        INotification notification = new Notification();
        notification.append(new NotificationErrorProps(message, PRODUCT_STR));
        return new ListProductOutput(null, notification);
    }

    public static ListProductOutput of(List<IProductEntity> products) {
        return new ListProductOutput(products, null);
    }

}
