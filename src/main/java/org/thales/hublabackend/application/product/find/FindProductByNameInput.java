package org.thales.hublabackend.application.product.find;

public record FindProductByNameInput(String productName) {
}
