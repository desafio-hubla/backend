package org.thales.hublabackend.application.product.create;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.product.find.FindProductByNameInput;
import org.thales.hublabackend.application.product.find.FindProductByNameUseCase;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.application.user.create.CreateUserUseCase;
import org.thales.hublabackend.application.user.find.FindUserByNameInput;
import org.thales.hublabackend.application.user.find.FindUserByNameOutput;
import org.thales.hublabackend.application.user.find.FindUserByNameUseCase;
import org.thales.hublabackend.domain.product.Product;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.Objects;

import static org.thales.hublabackend.shared.utils.ActionConstants.CREATE_PRODUCT;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;
import static org.thales.hublabackend.shared.utils.SuccessConstants.PRODUCT_INSERTED_ON_GATEWAY;
import static org.thales.hublabackend.shared.utils.SuccessConstants.PRODUCT_VALIDATED;

public class CreateProductUseCase {

    private static final ILog log = new Log(CreateUserUseCase.class);

    private final IProductGateway productGateway;
    private final FindProductByNameUseCase findProductByNameUseCase;
    private final FindUserByNameUseCase findUserByNameUseCase;

    private CreateProductUseCase(IProductGateway productGateway, FindProductByNameUseCase findProductByNameUseCase, FindUserByNameUseCase findUserByNameUseCase) {
        this.productGateway = Objects.requireNonNull(productGateway);
        this.findProductByNameUseCase = Objects.requireNonNull(findProductByNameUseCase);
        this.findUserByNameUseCase = Objects.requireNonNull(findUserByNameUseCase);
    }

    public static CreateProductUseCase of(IProductGateway productGateway, FindProductByNameUseCase findProductByNameUseCase, FindUserByNameUseCase findUserByNameUseCase) {
        return new CreateProductUseCase(productGateway, findProductByNameUseCase, findUserByNameUseCase);
    }

    public CreateProductOutput execute(CreateProductInput input) {
        IUserEntity producer = input.producer();
        String productName = input.productName();
        Integer productPrice = input.price();

        ProductDto product = this.createProduct(productName, productPrice, producer);

        if (product.notification().hasErrors()) {
            log.info(PRODUCT_VALIDATION_ERRORS, product.name(), product.notification().messages(""));
            return CreateProductOutput.of(product.notification());
        }

        log.info(PRODUCT_VALIDATED, product.name());

        if (this.hasNotProduct(product.name())) {
            String message = PRODUCT_NOT_FOUND.replace("{}", product.name());
            log.info(message);
            return CreateProductOutput.of(message);
        }

        return this.insertProduct(product);
    }

    private CreateProductOutput insertProduct(IProductEntity product) {
        try {
            this.productGateway.save(product);
            log.info(PRODUCT_INSERTED_ON_GATEWAY, product.name());
            return CreateProductOutput.of(product);
        } catch (Exception e) {
            log.error(e.getMessage());
            return  CreateProductOutput.of(GATEWAY_INSERT_STR.replace("{}", product.name()));
        }
    }

    private boolean hasNotProduct(String name) {
        return findProductByNameUseCase.execute(new FindProductByNameInput(name)).notification() == null;
    }

    private FindUserByNameOutput findUser(String userName) {
        return findUserByNameUseCase.execute(new FindUserByNameInput(userName));
    }

    private boolean isUserNotFound(IUserEntity user) {
        return user == null;
    }

    private ProductDto createProduct(String productName, Integer productPrice, IUserEntity user) {

        Product product = Product.with(productName, productPrice, (IUser) user);
        log.info(CREATE_PRODUCT, product.name());

        product.validate();

        return ProductDto.fromProduct(product);
    }

}
