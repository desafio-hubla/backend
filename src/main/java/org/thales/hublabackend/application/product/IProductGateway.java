package org.thales.hublabackend.application.product;

import java.util.List;
import java.util.Optional;

public interface IProductGateway<I extends IProductEntity, O extends IProductEntity> {

    public Optional<O> findProductByName(String productName);
    public List<O> findAll();
    public O save(I product);

}
