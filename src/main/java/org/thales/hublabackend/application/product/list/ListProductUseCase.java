package org.thales.hublabackend.application.product.list;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.application.product.find.FindProductByNameOutput;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.List;
import java.util.Objects;

import static org.thales.hublabackend.shared.utils.ActionConstants.FIND_ALL_PRODUCTS;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_ERROR_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.SuccessConstants.FOUND_X_PRODUCTS;

public class ListProductUseCase {

    private final IProductGateway productGateway;

    private static final ILog log = new Log(ListProductUseCase.class);

    private ListProductUseCase(IProductGateway gateway) {
        this.productGateway = Objects.requireNonNull(gateway);
    }

    public static ListProductUseCase of(IProductGateway productGateway) {
        return new ListProductUseCase(productGateway);
    }

    public ListProductOutput execute() {
        log.info(FIND_ALL_PRODUCTS);
        List<IProductEntity> products;

        try {
            products = this.productGateway.findAll();
        } catch (Exception e) {
            log.error(e.getMessage());
            return ListProductOutput.of(GATEWAY_ERROR_STR);
        }

        log.info(FOUND_X_PRODUCTS, products.size());
        return ListProductOutput.of(products);
    }

}
