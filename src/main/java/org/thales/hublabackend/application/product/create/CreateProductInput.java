package org.thales.hublabackend.application.product.create;

import org.thales.hublabackend.application.user.IUserEntity;

public record CreateProductInput(String productName, IUserEntity producer, Integer price) {
}
