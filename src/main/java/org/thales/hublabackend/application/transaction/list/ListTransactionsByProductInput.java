package org.thales.hublabackend.application.transaction.list;

public record ListTransactionsByProductInput(String productId) {
}
