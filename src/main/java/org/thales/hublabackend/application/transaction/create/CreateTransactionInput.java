package org.thales.hublabackend.application.transaction.create;

import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.user.IUser;

import java.time.Instant;

public record CreateTransactionInput(ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller) {
}
