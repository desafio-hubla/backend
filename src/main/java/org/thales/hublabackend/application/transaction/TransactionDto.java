package org.thales.hublabackend.application.transaction;

import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.transaction.ITransaction;
import org.thales.hublabackend.domain.transaction.Transaction;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.INotification;

import java.time.Instant;

public class TransactionDto implements ITransactionEntity {

    private String id;
    private ITypeTransaction kind;
    private Instant createdAt;
    private Integer total;
    private IProduct product;
    private IUser seller;
    private INotification notification;

    public TransactionDto() {
    }

    public TransactionDto(String id, ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller, INotification notification) {
        this.id = id;
        this.kind = kind;
        this.createdAt = createdAt;
        this.total = total;
        this.product = product;
        this.seller = seller;
        this.notification = notification;
    }

    public static TransactionDto fromTransaction(ITransaction transaction) {
        return new TransactionDto(transaction.id(), transaction.kind(), transaction.createdAt(), transaction.total(), ProductDto.fromProduct(transaction.product()), UserDto.fromUser(transaction.seller()), transaction.notification());
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public ITypeTransaction kind() {
        return kind;
    }

    @Override
    public Instant createdAt() {
        return createdAt;
    }

    @Override
    public Integer total() {
        return total;
    }

    @Override
    public IProduct product() {
        return product;
    }

    @Override
    public INotification notification() {
        return notification;
    }

    @Override
    public IUser seller() {
        return seller;
    }

    @Override
    public void changeBalance() {
        Transaction d = Transaction.of(id, kind, createdAt, total, product, seller);
        d.changeBalance();
        this.product = d.product();
        this.seller = d.seller();
        this.total = d.total();
    }
}
