package org.thales.hublabackend.application.transaction.find;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.Objects;
import java.util.Optional;

import static org.thales.hublabackend.shared.utils.ActionConstants.FIND_TRANSACTION_BY_ID;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.TRANSACTION_NOT_FOUND;
import static org.thales.hublabackend.shared.utils.SuccessConstants.TRANSACTION_FOUND;

public class FindTransactionByIdUseCase {

    private static final ILog log = new Log(FindTransactionByIdUseCase.class);

    private final ITransactionGateway transactionGateway;

    private FindTransactionByIdUseCase(ITransactionGateway transactionGateway) {
        this.transactionGateway = Objects.requireNonNull(transactionGateway);
    }

    public static FindTransactionByIdUseCase of(ITransactionGateway transactionGateway) {
        return new FindTransactionByIdUseCase(transactionGateway);
    }

    public FindTransactionByIdOutput execute(FindTransactionByIdInput input) {
        var id = input.transactionId();
        log.info(FIND_TRANSACTION_BY_ID, id);
        Optional<ITransactionEntity> transaction = Optional.empty();
        try {
            transaction = this.transactionGateway.findById(id);
        } catch (Exception e) {
            log.error(e.getMessage());
            return FindTransactionByIdOutput.of(GATEWAY_FIND_STR.replace("{}", id));
        }


        if (transaction.isEmpty()) {
            String message = TRANSACTION_NOT_FOUND.replace("{}", id);
            log.info(message);
            return FindTransactionByIdOutput.of(message);
        }

        log.info(TRANSACTION_FOUND, transaction.get().toString());
        return FindTransactionByIdOutput.of(transaction.get());
    }

}
