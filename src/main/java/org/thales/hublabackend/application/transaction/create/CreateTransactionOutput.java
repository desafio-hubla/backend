package org.thales.hublabackend.application.transaction.create;

import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;

import java.time.Instant;

import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;

public record CreateTransactionOutput(String id, ITypeTransaction kind, Instant createdAt, Integer total, IProduct product, IUser seller, INotification notification) {

    public static CreateTransactionOutput of(ITransactionEntity transaction) {
        return new CreateTransactionOutput(transaction.id(), transaction.kind(), transaction.createdAt(), transaction.total(), transaction.product(), transaction.seller(), transaction.notification());
    }

    public static CreateTransactionOutput of(INotification notification) {
        return new CreateTransactionOutput(null, null, null, null, null, null, notification);
    }

    public static CreateTransactionOutput of(String message) {
        INotification notification = new Notification();
        notification.append(message, TRANSACTION_STR);
        return new CreateTransactionOutput(null, null, null, null, null, null,  notification);
    }

}
