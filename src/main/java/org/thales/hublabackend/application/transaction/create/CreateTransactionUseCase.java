package org.thales.hublabackend.application.transaction.create;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.transaction.Transaction;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.time.Instant;
import java.util.Objects;

import static org.thales.hublabackend.shared.utils.ActionConstants.CREATE_TRANSACTION;
import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_INSERT_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.TRANSACTION_VALIDATION_ERRORS;
import static org.thales.hublabackend.shared.utils.SuccessConstants.TRANSACTION_INSERTED_ON_GATEWAY;
import static org.thales.hublabackend.shared.utils.SuccessConstants.TRANSACTION_VALIDATED;

public class CreateTransactionUseCase {

    private static final ILog log = new Log(CreateTransactionUseCase.class);

    private final ITransactionGateway transactionGateway;

    private CreateTransactionUseCase(ITransactionGateway transactionGateway) {
        this.transactionGateway = Objects.requireNonNull(transactionGateway);
    }

    public static CreateTransactionUseCase of(ITransactionGateway transactionGateway) {
        return new CreateTransactionUseCase(transactionGateway);
    }

    public CreateTransactionOutput execute(CreateTransactionInput input) {
        TransactionDto transaction = this.createTransaction(input);

        if (transaction.notification().hasErrors()) {
            log.info(TRANSACTION_VALIDATION_ERRORS, transaction.id(), transaction.notification().messages(""));
            return CreateTransactionOutput.of(transaction.notification());
        }

        log.info(TRANSACTION_VALIDATED, transaction.id());

        return this.insertTransaction(transaction);
    }

    private CreateTransactionOutput insertTransaction(TransactionDto transaction) {
        try {
            this.transactionGateway.save(transaction);
            log.info(TRANSACTION_INSERTED_ON_GATEWAY, transaction.id());
            return CreateTransactionOutput.of(transaction);
        } catch (Exception e) {
            log.error(e.getMessage());
            return CreateTransactionOutput.of(GATEWAY_INSERT_STR.replace("{}", TRANSACTION_STR.concat(": ").concat(transaction.id())));
        }
    }

    private TransactionDto createTransaction(CreateTransactionInput input) {
        final ITypeTransaction kind = input.kind();
        final Instant createdAt = input.createdAt();
        final IUserEntity seller = (IUserEntity) input.seller();
        final IProductEntity product = (IProductEntity) input.product();
        final Integer total = input.total();

        Transaction transaction = Transaction.with(kind, createdAt, total, product, seller);
        log.info(CREATE_TRANSACTION, transaction.id());

        transaction.validate();

        return TransactionDto.fromTransaction(transaction);
    }

}
