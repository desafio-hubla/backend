package org.thales.hublabackend.application.transaction.list;

import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;
import org.thales.hublabackend.shared.notification.NotificationErrorProps;

import java.util.List;

import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;

public record ListTransactionsByProductOutput(List<ITransactionEntity> transactions, INotification notification) {

    public static ListTransactionsByProductOutput of(String message) {
        INotification notification = new Notification();
        notification.append(new NotificationErrorProps(message, TRANSACTION_STR));
        return new ListTransactionsByProductOutput(null, notification);
    }

    public static ListTransactionsByProductOutput of(List<ITransactionEntity> transactions) {
        return new ListTransactionsByProductOutput(transactions, null);
    }

}
