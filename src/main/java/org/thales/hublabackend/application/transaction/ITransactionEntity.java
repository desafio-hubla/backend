package org.thales.hublabackend.application.transaction;

import org.thales.hublabackend.domain.transaction.ITransaction;

public interface ITransactionEntity extends ITransaction {
}
