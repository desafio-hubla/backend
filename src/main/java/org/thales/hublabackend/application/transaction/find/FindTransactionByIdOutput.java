package org.thales.hublabackend.application.transaction.find;

import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;
import org.thales.hublabackend.shared.notification.NotificationErrorProps;

import static org.thales.hublabackend.shared.utils.Constants.PRODUCT_STR;

public record FindTransactionByIdOutput(ITransactionEntity transaction, INotification notification) {

    public static FindTransactionByIdOutput of(String message) {
        INotification notification = new Notification();
        notification.append(new NotificationErrorProps(message, PRODUCT_STR));
        return new FindTransactionByIdOutput(null, notification);
    }

    public static FindTransactionByIdOutput of(ITransactionEntity transaction) {
        return new FindTransactionByIdOutput(transaction, null);
    }

}
