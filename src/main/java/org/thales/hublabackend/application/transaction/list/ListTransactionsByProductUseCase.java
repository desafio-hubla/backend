package org.thales.hublabackend.application.transaction.list;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.List;
import java.util.Objects;

import static org.thales.hublabackend.shared.utils.ActionConstants.FIND_TRANSACTION_BY_PRODUCT;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_ERROR_STR;
import static org.thales.hublabackend.shared.utils.SuccessConstants.FOUND_X_TRANSACTIONS;

public class ListTransactionsByProductUseCase {

    private static final ILog log = new Log(ListTransactionsByProductUseCase.class);

    private final ITransactionGateway transactionGateway;

    private ListTransactionsByProductUseCase(ITransactionGateway transactionGateway) {
        this.transactionGateway = Objects.requireNonNull(transactionGateway);
    }

    public static ListTransactionsByProductUseCase of(ITransactionGateway gateway) {
        return new ListTransactionsByProductUseCase(gateway);
    }

    public ListTransactionsByProductOutput execute(ListTransactionsByProductInput input) {
        var productId = input.productId();
        log.info(FIND_TRANSACTION_BY_PRODUCT, productId);

        List<ITransactionEntity> transactions;
        try {
            transactions = this.transactionGateway.findByProduct(productId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return ListTransactionsByProductOutput.of(GATEWAY_ERROR_STR);
        }

        log.info(FOUND_X_TRANSACTIONS, transactions.size());
        return ListTransactionsByProductOutput.of(transactions);
    }
}
