package org.thales.hublabackend.application.transaction.find;

public record FindTransactionByIdInput(String transactionId) {
}
