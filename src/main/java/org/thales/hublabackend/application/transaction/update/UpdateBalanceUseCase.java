package org.thales.hublabackend.application.transaction.update;

import org.springframework.beans.factory.annotation.Autowired;
import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.application.transaction.create.CreateTransactionInput;
import org.thales.hublabackend.application.transaction.create.CreateTransactionOutput;
import org.thales.hublabackend.application.transaction.create.CreateTransactionUseCase;
import org.thales.hublabackend.application.transaction.find.FindTransactionByIdInput;
import org.thales.hublabackend.application.transaction.find.FindTransactionByIdUseCase;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.domain.user.User;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import static org.thales.hublabackend.shared.utils.Constants.TRANSACTION_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;
import static org.thales.hublabackend.shared.utils.SuccessConstants.*;

public class UpdateBalanceUseCase {

    private static final ILog log = new Log(UpdateBalanceUseCase.class);

    @Autowired
    FindTransactionByIdUseCase findTransactionByIdUseCase;

    private final ITransactionGateway transactionGateway;

    private UpdateBalanceUseCase(ITransactionGateway transactionGateway) {
        this.transactionGateway = transactionGateway;
    }

    public static UpdateBalanceUseCase of(ITransactionGateway transactionGateway) {
        return new UpdateBalanceUseCase(transactionGateway);
    }

    public UpdateBalanceOutput execute(UpdateBalanceInput input) {
        String id = input.transactionId();
        var findTransactionByIdOutput = findTransactionByIdUseCase.execute(new FindTransactionByIdInput(id));

        var transaction = findTransactionByIdOutput.transaction();
        var dto = TransactionDto.fromTransaction(transaction);
        dto.changeBalance();

        try {
            var updatedTransaction = this.transactionGateway.save(dto);
            log.info(CHANGED_BALANCE_ON_GATEWAY, transaction.id());
            return UpdateBalanceOutput.of(updatedTransaction);
        } catch (Exception e) {
            log.error(e.getMessage());
            return UpdateBalanceOutput.of(GATEWAY_UPDATE_STR.replace("{}", TRANSACTION_STR.concat(": ").concat(transaction.id())));
        }
    }

}
