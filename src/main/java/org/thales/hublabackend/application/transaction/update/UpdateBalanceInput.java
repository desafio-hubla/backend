package org.thales.hublabackend.application.transaction.update;

public record UpdateBalanceInput(String transactionId) {
}
