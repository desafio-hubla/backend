package org.thales.hublabackend.application.transaction;

import org.thales.hublabackend.infrastructure.gateway.transaction.TransactionJpaEntity;

import java.util.List;
import java.util.Optional;

public interface ITransactionGateway<O extends ITransactionEntity> {

    public O save(TransactionDto transaction);
    public List<O> findByProduct(String productId);
    public Optional<O> findById(String id);
    public List<TransactionJpaEntity> findAll();
}
