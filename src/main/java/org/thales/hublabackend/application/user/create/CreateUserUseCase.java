package org.thales.hublabackend.application.user.create;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.application.user.IUserGateway;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.application.user.find.FindUserByNameInput;
import org.thales.hublabackend.application.user.find.FindUserByNameUseCase;
import org.thales.hublabackend.domain.enumerator.user.ITypeUser;
import org.thales.hublabackend.domain.user.User;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.Objects;

import static org.thales.hublabackend.shared.utils.ActionConstants.CREATE_USER;
import static org.thales.hublabackend.shared.utils.ErrorConstants.*;
import static org.thales.hublabackend.shared.utils.SuccessConstants.USER_INSERTED_ON_GATEWAY;
import static org.thales.hublabackend.shared.utils.SuccessConstants.USER_VALIDATED;

public class CreateUserUseCase {

    private static final ILog log = new Log(CreateUserUseCase.class);

    private final IUserGateway userGateway;
    private final FindUserByNameUseCase findUserByNameUseCase;

    private CreateUserUseCase(IUserGateway userGateway, FindUserByNameUseCase findUserByNameUseCase) {
        this.userGateway = Objects.requireNonNull(userGateway);
        this.findUserByNameUseCase = findUserByNameUseCase;
    }

    public static CreateUserUseCase of(IUserGateway userGateway, FindUserByNameUseCase findUserByNameUseCase) {
        return new CreateUserUseCase(userGateway, findUserByNameUseCase);
    }

    public CreateUserOutput execute(CreateUserInput createUserInput) {
        UserDto user = this.createUser(createUserInput);

        if (user.notification().hasErrors()) {
            log.info(USER_VALIDATION_ERRORS, user.name(), user.notification().messages(""));
            return CreateUserOutput.of(user.notification());
        }

        log.info(USER_VALIDATED, user.name());

        if (!this.hasUser(user.name())) {
            String message = USER_ALREADY_EXISTS.replace("{}", user.name());
            log.info(message);
            return CreateUserOutput.of(message);
        }

        return this.insertUser(user);
    }

    private CreateUserOutput insertUser(IUserEntity user) {
        try {
            this.userGateway.save(user);
            log.info(USER_INSERTED_ON_GATEWAY, user.name());
            return CreateUserOutput.of(user);
        } catch (Exception e) {
            log.error(e.getMessage());
            return  CreateUserOutput.of(GATEWAY_INSERT_STR.replace("{}", user.name()));
        }
    }

    private boolean hasUser(String name) {
        return findUserByNameUseCase.execute(new FindUserByNameInput(name)).notification() != null;
    }

    private UserDto createUser(CreateUserInput createUserInput) {
        final String name = createUserInput.name();
        final ITypeUser type = createUserInput.typeUser();

        User user = User.with(name, type);
        log.info(CREATE_USER, user.name());

        user.validate();

        return UserDto.fromUser(user);
    }

}
