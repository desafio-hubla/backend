package org.thales.hublabackend.application.user.create;

import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;

import static org.thales.hublabackend.shared.utils.Constants.USER_STR;

public record CreateUserOutput(String id, String name, Integer type, Integer balance, INotification notificationErrors) {

    public static CreateUserOutput of(IUserEntity user) {
        return new CreateUserOutput(user.id(), user.name(), user.type().get(), user.balance(), null);
    }

    public static CreateUserOutput of(INotification notification) {
        return new CreateUserOutput(null, null, null, null, notification);
    }

    public static CreateUserOutput of(String message) {
        INotification notification = new Notification();
        notification.append(message, USER_STR);
        return new CreateUserOutput(null, null, null, null, notification);
    }

}
