package org.thales.hublabackend.application.user;

import org.thales.hublabackend.application.user.create.CreateUserOutput;
import org.thales.hublabackend.domain.enumerator.user.ITypeUser;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;
import org.thales.hublabackend.domain.product.Product;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.domain.user.User;
import org.thales.hublabackend.shared.notification.INotification;

public class UserDto implements IUserEntity {

    private String id;
    private String name;
    private ITypeUser type;
    private Integer balance;
    private INotification notification;

    public UserDto(String id, String name, ITypeUser type, Integer balance, INotification notification) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.balance = balance;
        this.notification = notification;
    }

    public static UserDto fromUser(IUser user) {
        return new UserDto(user.id(), user.name(), user.type(), user.balance(), user.notification());
    }

    public static UserDto fromCreateUser(CreateUserOutput output) {
        return new UserDto(output.id(), output.name(), TypeUser.getByTypeOrNull(output.type()), output.balance(), output.notificationErrors());
    }

    @Override
    public void receiveComission(Integer comission) {
        User user = User.with(id, name, type, balance);
        user.receiveComission(comission);

        this.balance = user.balance();
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Integer balance() {
        return balance;
    }

    @Override
    public INotification notification() {
        return notification;
    }

    @Override
    public ITypeUser type() {
        return type;
    }


}
