package org.thales.hublabackend.application.user;

import java.util.Optional;

public interface IUserGateway<I extends IUserEntity, O extends IUserEntity> {

    Optional<O> findUserByName(String userName);
    O save(I user);

}
