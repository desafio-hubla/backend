package org.thales.hublabackend.application.user.find;

import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.shared.notification.INotification;
import org.thales.hublabackend.shared.notification.Notification;
import org.thales.hublabackend.shared.notification.NotificationErrorProps;

import static org.thales.hublabackend.shared.utils.Constants.USER_STR;

public record FindUserByNameOutput(IUserEntity user, INotification notification) {

    public static FindUserByNameOutput of(String message) {
        INotification notification = new Notification();
        notification.append(new NotificationErrorProps(message, USER_STR));
        return new FindUserByNameOutput(null, notification);
    }

    public static FindUserByNameOutput of(IUserEntity user) {
        return new FindUserByNameOutput(user, null);
    }

}
