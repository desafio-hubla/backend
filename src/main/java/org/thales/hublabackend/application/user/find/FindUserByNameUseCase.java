package org.thales.hublabackend.application.user.find;

import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.application.user.IUserGateway;
import org.thales.hublabackend.shared.log.ILog;
import org.thales.hublabackend.shared.log.Log;

import java.util.Objects;
import java.util.Optional;

import static org.thales.hublabackend.shared.utils.ActionConstants.FIND_USER_BY_NAME;
import static org.thales.hublabackend.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static org.thales.hublabackend.shared.utils.ErrorConstants.USER_NOT_FOUND;
import static org.thales.hublabackend.shared.utils.SuccessConstants.USER_FOUND;

public class FindUserByNameUseCase {

    private static final ILog log = new Log(FindUserByNameUseCase.class);

    private final IUserGateway userGateway;

    private FindUserByNameUseCase(IUserGateway userGateway) {
        this.userGateway = Objects.requireNonNull(userGateway);
    }

    public static FindUserByNameUseCase of(IUserGateway gateway) {
        return new FindUserByNameUseCase(gateway);
    }

    public FindUserByNameOutput execute(FindUserByNameInput input) {
        var name = input.userName();
        log.info(FIND_USER_BY_NAME, name);
        Optional<IUserEntity> user = Optional.empty();
        try {
            user = this.userGateway.findUserByName(name);
        } catch (Exception e) {
            log.error(e.getMessage());
            return FindUserByNameOutput.of(GATEWAY_FIND_STR.replace("{}", name));
        }


        if (user.isEmpty()) {
            String message = USER_NOT_FOUND.replace("{}", name);
            log.info(message);
            return FindUserByNameOutput.of(message);
        }

        log.info(USER_FOUND, user.get().toString());
        return FindUserByNameOutput.of(user.get());
    }

}
