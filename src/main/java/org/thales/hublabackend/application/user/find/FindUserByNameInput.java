package org.thales.hublabackend.application.user.find;

public record FindUserByNameInput(String userName) {
}
