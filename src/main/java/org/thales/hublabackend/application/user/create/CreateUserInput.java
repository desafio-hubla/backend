package org.thales.hublabackend.application.user.create;

import org.thales.hublabackend.domain.enumerator.user.TypeUser;

public record CreateUserInput(String name, TypeUser typeUser) {
}
