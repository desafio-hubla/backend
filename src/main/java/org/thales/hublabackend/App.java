package org.thales.hublabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.thales.hublabackend.infrastructure.config.WebServerConfig;

@SpringBootApplication
public class App {

	public static void main(String[] args) {
		SpringApplication.run(WebServerConfig.class, args);
	}

}
