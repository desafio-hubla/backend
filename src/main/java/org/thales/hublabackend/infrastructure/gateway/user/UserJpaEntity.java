package org.thales.hublabackend.infrastructure.gateway.user;

import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.enumerator.user.ITypeUser;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;
import org.thales.hublabackend.shared.notification.INotification;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UserJpaEntity implements IUserEntity {

  @Id
  private String id;

  @Column(name = "name", length = 30, nullable = false)
  private String name;

  @Column(name = "type", nullable = false)
  private Integer type;

  @Column(name = "balance", nullable = false)
  private Integer balance;

  private UserJpaEntity() {
  }

  public UserJpaEntity(String id, String name, Integer type) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.balance = 0;
  }

  public UserJpaEntity(String id, String name, Integer type, Integer balance) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.balance = balance;
  }

  public static UserJpaEntity of(UserDto userDto) {
    return new UserJpaEntity(userDto.id(), userDto.name(), userDto.type().get(), userDto.balance());
  }

  public void receiveComission(Integer comission) {
    this.balance += comission;
  }

  @Override
  public String id() {
    return id;
  }

  @Override
  public String name() {
    return name;
  }

  @Override
  public Integer balance() {
    return balance;
  }

  @Override
  public INotification notification() {
    return null;
  }

  @Override
  public ITypeUser type() {
    return TypeUser.getByTypeOrNull(type);
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public Integer getBalance() {
    return balance;
  }

  public Integer getType() {
    return type;
  }
}
