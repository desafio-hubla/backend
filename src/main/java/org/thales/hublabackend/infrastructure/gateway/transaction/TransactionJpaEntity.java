package org.thales.hublabackend.infrastructure.gateway.transaction;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.domain.enumerator.transaction.ITypeTransaction;
import org.thales.hublabackend.domain.enumerator.transaction.TypeTransaction;
import org.thales.hublabackend.domain.product.IProduct;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.infrastructure.gateway.product.ProductJpaEntity;
import org.thales.hublabackend.infrastructure.gateway.transaction.output.TransactionOutput;
import org.thales.hublabackend.infrastructure.gateway.user.UserJpaEntity;
import org.thales.hublabackend.shared.notification.INotification;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "transactions")
public class TransactionJpaEntity implements ITransactionEntity {

    @Id
    private String id;

    @Column(name = "kind", nullable = false)
    private Integer kind;

    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private ProductJpaEntity product;

    @Column(name = "total", nullable = false)
    private Integer total;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "seller_id")
    private UserJpaEntity seller;

    public TransactionJpaEntity() {
    }

    public TransactionJpaEntity(String id, Integer kind, Instant createdAt, ProductJpaEntity product, Integer total, UserJpaEntity seller) {
        this.id = id;
        this.kind = kind;
        this.createdAt = createdAt;
        this.product = product;
        this.total = total;
        this.seller = seller;
    }

    public static TransactionJpaEntity of(TransactionDto transactionDto, ProductJpaEntity product, UserJpaEntity seller) {
        return new TransactionJpaEntity(transactionDto.id(), transactionDto.kind().getType(), transactionDto.createdAt(), product, transactionDto.total(), seller);
    }

    public static TransactionJpaEntity of(TransactionOutput o) {
        return new TransactionJpaEntity(o.getId(), Integer.parseInt(o.getKind()), o.getCreatedAt(), o.getProduct(), o.getTotal(), o.getSeller());
    }

    public String getId() {
        return id;
    }

    public Integer getKind() {
        return kind;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public ProductJpaEntity getProduct() {
        return product;
    }

    public Integer getTotal() {
        return total;
    }

    public UserJpaEntity getSeller() {
        return seller;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public ITypeTransaction kind() {
        return TypeTransaction.getByTypeOrNull(kind);
    }

    @Override
    public Instant createdAt() {
        return createdAt;
    }

    @Override
    public Integer total() {
        return total;
    }

    @Override
    public IProduct product() {
        return product;
    }

    @Override
    public INotification notification() {
        return null;
    }

    @Override
    public IUser seller() {
        return seller;
    }

    @Override
    public void changeBalance() {
        return;
    }
}
