package org.thales.hublabackend.infrastructure.gateway.product;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ProductRepository extends JpaRepository<ProductJpaEntity, String> {

//    @Query(value = "SELECT DISTINCT p.name, p.price FROM ProductJpaEntity p")
//    Page<ProductJpaEntity> findAll(Pageable page);
//
//    @Query(value = "SELECT DISTINCT p.seller.name, p.seller.isProducer FROM ProductJpaEntity p where p.name = ?1")
//    List<Map> findAllBySeller(String product);
//
    Optional<ProductJpaEntity> findByName(String name);
}
