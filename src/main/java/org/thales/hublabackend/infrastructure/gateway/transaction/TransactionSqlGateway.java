package org.thales.hublabackend.infrastructure.gateway.transaction;

import org.springframework.stereotype.Component;
import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.ITransactionGateway;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.application.user.IUserEntity;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.infrastructure.gateway.product.ProductJpaEntity;
import org.thales.hublabackend.infrastructure.gateway.transaction.output.TransactionOutput;
import org.thales.hublabackend.infrastructure.gateway.user.UserJpaEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
public class TransactionSqlGateway implements ITransactionGateway<TransactionJpaEntity> {

    private final TransactionRepository transactionRepository;

    public TransactionSqlGateway(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public TransactionJpaEntity save(TransactionDto transactionDto) {
        var seller = UserJpaEntity.of((UserDto) transactionDto.seller());
        var product = ProductJpaEntity.of((ProductDto) transactionDto.product(), (UserJpaEntity) transactionDto.product().producer());
        var transaction = TransactionJpaEntity.of(transactionDto, product, seller);
        return this.transactionRepository.save(transaction);
    }

    public List<TransactionJpaEntity> findByProduct(String productId) {
        List<TransactionOutput> output = this.transactionRepository.findAllByProduct(productId);
        return output.stream().map(o -> TransactionJpaEntity.of(o)).collect(Collectors.toList());
    }

    @Override
    public Optional<TransactionJpaEntity> findById(String id) {
        return this.transactionRepository.findById(id);
    }


    @Override
    public List<TransactionJpaEntity> findAll() {
        return this.transactionRepository.findAll();
    }

}
