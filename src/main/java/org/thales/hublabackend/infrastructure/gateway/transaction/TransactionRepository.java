package org.thales.hublabackend.infrastructure.gateway.transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.thales.hublabackend.infrastructure.gateway.transaction.output.TransactionOutput;

import java.util.List;

public interface TransactionRepository extends JpaRepository<TransactionJpaEntity, String> {

    @Query("SELECT t.id as id, t.createdAt as createdAt, t.kind as kind, t.total as total, t.seller as seller, t.product as product FROM TransactionJpaEntity t INNER JOIN t.product p where p.id = ?1")
    List<TransactionOutput> findAllByProduct(String id);

}
