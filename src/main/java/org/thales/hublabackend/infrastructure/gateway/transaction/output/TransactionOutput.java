package org.thales.hublabackend.infrastructure.gateway.transaction.output;

import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.infrastructure.gateway.product.ProductJpaEntity;
import org.thales.hublabackend.infrastructure.gateway.user.UserJpaEntity;

import java.time.Instant;

public interface TransactionOutput extends ITransactionEntity {

    public String getId();
    public Instant getCreatedAt();
    public String getKind();
    public Integer getTotal();
    public UserJpaEntity getSeller();
    public ProductJpaEntity getProduct();

}
