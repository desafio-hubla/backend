package org.thales.hublabackend.infrastructure.gateway.product;

import org.thales.hublabackend.application.product.IProductEntity;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.domain.user.IUser;
import org.thales.hublabackend.infrastructure.gateway.user.UserJpaEntity;
import org.thales.hublabackend.shared.notification.INotification;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class ProductJpaEntity implements IProductEntity {

  @Id
  private String id;

  @Column(name = "name", length = 30, nullable = false)
  private String name;

  @Column(name = "price", nullable = false)
  private Integer price;

  @Column(name = "sells", nullable = false)
  private Integer sells;

  @Column(name = "amount_sold", nullable = false)
  private Integer amountSold;

  @OneToOne(cascade = CascadeType.PERSIST)
  @JoinColumn(name = "producer_id")
  private UserJpaEntity producer;

  public void sold() {
    this.sells += this.price;
  }

  private ProductJpaEntity(String id, String name, UserJpaEntity producer, Integer price) {
    this.id = id;
    this.name = name;
    this.producer = producer;
    this.price = price;
    this.sells = 0;
    this.amountSold = 0;
  }

  private ProductJpaEntity(String id, String name, UserJpaEntity producer, Integer price, Integer sells, Integer amountSold) {
    this.id = id;
    this.name = name;
    this.producer = producer;
    this.price = price;
    this.sells = sells;
    this.amountSold = amountSold;
  }

  private ProductJpaEntity() {
  }

  public static ProductJpaEntity of(ProductDto productDto, UserJpaEntity user) {
    return new ProductJpaEntity(productDto.id(), productDto.name(), user, productDto.price(), productDto.sells(), productDto.amountSold());
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public UserJpaEntity getProducer() {
    return producer;
  }

  public Integer getPrice() {
    return price;
  }

  @Override
  public String id() {
    return id;
  }

  @Override
  public String name() {
    return name;
  }

  @Override
  public INotification notification() {
    return null;
  }

  @Override
  public Integer price() {
    return price;
  }

  @Override
  public Integer sells() {
    return sells;
  }

  @Override
  public IUser producer() {
    return producer;
  }

  @Override
  public Integer amountSold() {
    return amountSold;
  }
}
