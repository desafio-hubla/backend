package org.thales.hublabackend.infrastructure.gateway.user;

import org.springframework.stereotype.Component;
import org.thales.hublabackend.application.user.IUserGateway;
import org.thales.hublabackend.application.user.UserDto;

import java.util.List;
import java.util.Optional;

@Component
public class UserSqlGateway implements IUserGateway<UserDto, UserJpaEntity> {

    private final UserRepository repository;

    public UserSqlGateway(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<UserJpaEntity> findUserByName(String userName) {
        return this.repository.findByName(userName);
    }

    @Override
    public UserJpaEntity save(UserDto user) {
        return this.repository.save(UserJpaEntity.of(user));
    }
}
