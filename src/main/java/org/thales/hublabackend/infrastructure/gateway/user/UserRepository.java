package org.thales.hublabackend.infrastructure.gateway.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserJpaEntity, String> {

    @Query("FROM UserJpaEntity u where u.name = ?1")
    Optional<UserJpaEntity> findByName(String name);
}
