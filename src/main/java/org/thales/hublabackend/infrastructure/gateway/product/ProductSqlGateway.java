package org.thales.hublabackend.infrastructure.gateway.product;

import org.springframework.stereotype.Component;
import org.thales.hublabackend.application.product.IProductGateway;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.infrastructure.gateway.user.UserJpaEntity;

import java.util.List;
import java.util.Optional;

@Component
public class ProductSqlGateway implements IProductGateway<ProductDto, ProductJpaEntity> {

    private final ProductRepository repository;

    public ProductSqlGateway(ProductRepository repository) {
        this.repository = repository;
    }
    @Override
    public Optional<ProductJpaEntity> findProductByName(String productName) {
        return this.repository.findByName(productName);
    }

    public List<ProductJpaEntity> findAll() {
        return this.repository.findAll();
    }

    @Override
    public ProductJpaEntity save(ProductDto product) {
        return this.repository.save(ProductJpaEntity.of(product, UserJpaEntity.of((UserDto) product.producer())));
    }
}
