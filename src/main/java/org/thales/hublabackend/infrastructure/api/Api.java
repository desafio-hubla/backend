package org.thales.hublabackend.infrastructure.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequestMapping(value = "api/v1")
@Tag(name = "api/v1")
public interface Api {

  @PostMapping(
          consumes = { "multipart/form-data" },
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Create transactions from file")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "201", description = "Created successfully"),
          @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> create(@RequestParam("files") List<MultipartFile> file) throws Exception;


  @GetMapping
  @Operation(summary = "List all products")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Listed successfully"),
          @ApiResponse(responseCode = "422", description = "A invalid parameter was received"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> listProducts();

  @GetMapping(
          value = "{id}",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Find transaction by product id")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Successfully found"),
          @ApiResponse(responseCode = "422", description = "A invalid parameter was received"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> findTransactionByProduct(@PathVariable(name = "id") String id);

}

