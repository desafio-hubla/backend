package org.thales.hublabackend.infrastructure.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.thales.hublabackend.application.product.ProductDto;
import org.thales.hublabackend.application.product.create.CreateProductInput;
import org.thales.hublabackend.application.product.create.CreateProductUseCase;
import org.thales.hublabackend.application.product.find.FindProductByNameInput;
import org.thales.hublabackend.application.product.find.FindProductByNameOutput;
import org.thales.hublabackend.application.product.find.FindProductByNameUseCase;
import org.thales.hublabackend.application.transaction.TransactionDto;
import org.thales.hublabackend.application.transaction.create.CreateTransactionInput;
import org.thales.hublabackend.application.transaction.create.CreateTransactionUseCase;
import org.thales.hublabackend.application.transaction.update.UpdateBalanceInput;
import org.thales.hublabackend.application.transaction.update.UpdateBalanceUseCase;
import org.thales.hublabackend.application.user.UserDto;
import org.thales.hublabackend.application.user.create.CreateUserInput;
import org.thales.hublabackend.application.user.create.CreateUserOutput;
import org.thales.hublabackend.application.user.create.CreateUserUseCase;
import org.thales.hublabackend.application.user.find.FindUserByNameInput;
import org.thales.hublabackend.application.user.find.FindUserByNameOutput;
import org.thales.hublabackend.application.user.find.FindUserByNameUseCase;
import org.thales.hublabackend.domain.enumerator.transaction.TypeTransaction;
import org.thales.hublabackend.domain.enumerator.user.ITypeUser;
import org.thales.hublabackend.domain.enumerator.user.TypeUser;

import java.net.URI;
import java.time.Instant;
import java.util.*;

@Service
public class CreateTransactionsFromFileService {

    @Autowired
    private CreateUserUseCase createUserUseCase;

    @Autowired
    private CreateProductUseCase createProductUseCase;

    @Autowired
    private CreateTransactionUseCase createTransactionUseCase;

    @Autowired
    private FindProductByNameUseCase findProductByNameUseCase;

    @Autowired
    private FindUserByNameUseCase findUserByNameUseCase;

    @Autowired
    private UpdateBalanceUseCase updateBalanceUseCase;

    private final Map<String, List<String>> producers = new HashMap();
    private final Map<String, String> affiliateds = new HashMap<>();
    private final Map<String, String> products = new HashMap<>();
    private final Map<String, List<String>> transactions = new HashMap();

    public void execute(String[] rows) throws Exception {
        this.populateAttributes(rows);
        this.createProducersAndProducts();
        this.createAffiliateds();
        this.createTransactionsAndUpdateBalance();
    }

    private void createTransactionsAndUpdateBalance() {
        for (String transactionRef : transactions.keySet()) {
            String kind = transactions.get(transactionRef).get(0);
            String date = transactions.get(transactionRef).get(1);
            String productName = transactions.get(transactionRef).get(2);
            String userName = transactions.get(transactionRef).get(3);
            String price = transactions.get(transactionRef).get(4);

            var kindValidation = this.validateTransaction(kind);
            if(kindValidation == null)
                continue;

            final var findProductByNameOutput = findProductByNameUseCase.execute(new FindProductByNameInput(productName));
            final var findUserByNameOutput = findUserByNameUseCase.execute(new FindUserByNameInput(userName));
            if(findUserByNameOutput.notification() != null)
                continue;
            final var createTransactionOutput = createTransactionUseCase.execute(buildCreateTransactionInput(kind, date, price, findProductByNameOutput, findUserByNameOutput));

            updateBalanceUseCase.execute(new UpdateBalanceInput(createTransactionOutput.id()));
        }
    }

    private TypeTransaction validateTransaction(String kind) {
        Integer kindInt;
        try {
            kindInt = Integer.parseInt(kind);
        } catch (Exception e) {
            return null;
        }

        return TypeTransaction.getByTypeOrNull(kindInt);
    }

    private static CreateTransactionInput buildCreateTransactionInput(String kind, String date, String price, FindProductByNameOutput findProductByNameOutput, FindUserByNameOutput findUserByNameOutput) {
        return new CreateTransactionInput(
                TypeTransaction.getByTypeOrNull(Integer.parseInt(kind)),
                Instant.parse(date),
                Integer.parseInt(price),
                ProductDto.fromProduct(findProductByNameOutput.product()),
                UserDto.fromUser(findUserByNameOutput.user())
        );
    }

    private void createAffiliateds() {
        for (String affiliated : affiliateds.keySet()) {
            this.insertUser(affiliated, TypeUser.AFFILIATED);
        }
    }

    private void createProducersAndProducts() {
        for (String productName : producers.keySet()) {
            var userName = producers.get(productName).get(0);
            var productPrice = producers.get(productName).get(1);

            var user = this.insertUser(userName, TypeUser.PRODUCER);
            if(user.notificationErrors() == null)
                this.insertProduct(productName, productPrice, user);
        }
    }

    private void insertProduct(String productName, String productPrice, CreateUserOutput user) {
        createProductUseCase.execute(new CreateProductInput(productName, UserDto.fromCreateUser(user), Integer.parseInt(productPrice.trim())));
    }

    private CreateUserOutput insertUser(String userName, ITypeUser iTypeUser) {
        return createUserUseCase.execute(new CreateUserInput(userName, (TypeUser) iTypeUser));
    }

    private void populateAttributes(String[] rows) {
        for (String row : rows) {
            String kind = row.trim().substring(0, 1);
            String productName = row.trim().substring(26, 56).trim();
            String userName = row.trim().substring(66, row.length());
            String price = row.trim().substring(56, 66);
            String date = row.trim().substring(1, 26);

            if (kind.equals("1"))
                producers.put(productName, List.of(userName, price));
            else if (kind.equals("2"))
                affiliateds.put(userName, userName);

            products.put(productName, userName);

            transactions.put(UUID.randomUUID().toString(), List.of(kind, date, productName, userName, price));
        }
    }

}
