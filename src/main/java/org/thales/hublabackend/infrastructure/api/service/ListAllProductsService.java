package org.thales.hublabackend.infrastructure.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.thales.hublabackend.application.product.list.ListProductOutput;
import org.thales.hublabackend.application.product.list.ListProductUseCase;

import java.net.URI;

@Service
public class ListAllProductsService {

    @Autowired
    private ListProductUseCase listProductUseCase;

    public ListProductOutput execute() {
        return this.listProductUseCase.execute();
    }

}
