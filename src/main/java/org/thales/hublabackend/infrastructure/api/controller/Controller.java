package org.thales.hublabackend.infrastructure.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.thales.hublabackend.infrastructure.api.Api;
import org.thales.hublabackend.infrastructure.api.service.CreateTransactionsFromFileService;
import org.thales.hublabackend.infrastructure.api.service.ListAllProductsService;
import org.thales.hublabackend.infrastructure.api.service.ListAllTransactionsByProductService;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class Controller implements Api {

  @Autowired
  CreateTransactionsFromFileService createTransactionsFromFileService;

  @Autowired
  ListAllProductsService listAllProductsService;

  @Autowired
  ListAllTransactionsByProductService listAllTransactionsByProductService;

  @Override
  public ResponseEntity<?> create(List<MultipartFile> files) throws Exception {
    MultipartFile file = files.get(0);

    String content = new String(file.getBytes(), StandardCharsets.UTF_8).trim();
    String[] rows = content.split("\n");

    try {
      createTransactionsFromFileService.execute(rows);
    } catch (Exception e) {
      return ResponseEntity.internalServerError().build();
    }

    return ResponseEntity.created(URI.create("/products")).body("");
  }

  @Override
  public ResponseEntity<?> listProducts() {
    var products = listAllProductsService.execute();
    return ResponseEntity.created(URI.create("")).body(products);
  }

  @Override
  public ResponseEntity<?> findTransactionByProduct(String id) {
    var result = listAllTransactionsByProductService.execute(id);
    return ResponseEntity.created(URI.create("")).body(
            result.transactions().stream().collect(
                    Collectors.groupingBy(transactionOutput -> transactionOutput.seller().name())
            )
    );
  }

}
