package org.thales.hublabackend.infrastructure.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thales.hublabackend.application.transaction.list.ListTransactionsByProductInput;
import org.thales.hublabackend.application.transaction.list.ListTransactionsByProductOutput;
import org.thales.hublabackend.application.transaction.list.ListTransactionsByProductUseCase;

@Service
public class ListAllTransactionsByProductService {

    @Autowired
    private ListTransactionsByProductUseCase listTransactionsByProductUseCase;

    public ListTransactionsByProductOutput execute(String productId) {
        return this.listTransactionsByProductUseCase.execute(new ListTransactionsByProductInput(productId));
    }

}
