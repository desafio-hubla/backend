package org.thales.hublabackend.infrastructure.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thales.hublabackend.application.product.create.CreateProductUseCase;
import org.thales.hublabackend.application.product.find.FindProductByNameUseCase;
import org.thales.hublabackend.application.product.list.ListProductUseCase;
import org.thales.hublabackend.application.transaction.ITransactionEntity;
import org.thales.hublabackend.application.transaction.create.CreateTransactionUseCase;
import org.thales.hublabackend.application.transaction.find.FindTransactionByIdUseCase;
import org.thales.hublabackend.application.transaction.list.ListTransactionsByProductUseCase;
import org.thales.hublabackend.application.transaction.update.UpdateBalanceUseCase;
import org.thales.hublabackend.application.user.create.CreateUserUseCase;
import org.thales.hublabackend.application.user.find.FindUserByNameUseCase;
import org.thales.hublabackend.infrastructure.gateway.product.ProductSqlGateway;
import org.thales.hublabackend.infrastructure.gateway.transaction.TransactionSqlGateway;
import org.thales.hublabackend.infrastructure.gateway.user.UserSqlGateway;

@Configuration
public class ContextConfig {

  private final TransactionSqlGateway transactionSqlGateway;
  private final ProductSqlGateway productSqlGateway;
  private final UserSqlGateway userSqlGateway;

  public ContextConfig(TransactionSqlGateway transactionSqlGateway, ProductSqlGateway productSqlGateway, UserSqlGateway userSqlGateway) {
    this.transactionSqlGateway = transactionSqlGateway;
    this.productSqlGateway = productSqlGateway;
    this.userSqlGateway = userSqlGateway;
  }

  @Bean
  public CreateTransactionUseCase createTransactionUseCase() {
    return CreateTransactionUseCase.of(transactionSqlGateway);
  }

  @Bean
  public CreateUserUseCase createUserUseCase() {
    return CreateUserUseCase.of(userSqlGateway, findUserByNameUseCase());
  }

  @Bean
  public CreateProductUseCase createProductUseCase() {
    return CreateProductUseCase.of(productSqlGateway, findProductByNameUseCase(), findUserByNameUseCase());
  }

  @Bean
  public FindProductByNameUseCase findProductByNameUseCase() {
    return FindProductByNameUseCase.of(productSqlGateway);
  }

  @Bean
  public FindUserByNameUseCase findUserByNameUseCase() {
    return FindUserByNameUseCase.of(userSqlGateway);
  }

  @Bean
  public UpdateBalanceUseCase updateBalanceUseCase() {
    return UpdateBalanceUseCase.of(transactionSqlGateway);
  }

  @Bean
  public FindTransactionByIdUseCase findTransactionByIdUseCase() {
    return FindTransactionByIdUseCase.of(transactionSqlGateway);
  }

  @Bean
  public ListProductUseCase listProductUseCase() {
    return ListProductUseCase.of(productSqlGateway);
  }

  @Bean
  public ListTransactionsByProductUseCase listTransactionsByProductUseCase() {
    return ListTransactionsByProductUseCase.of(transactionSqlGateway);
  }

}
