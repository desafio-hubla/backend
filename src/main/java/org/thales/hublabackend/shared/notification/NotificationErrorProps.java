package org.thales.hublabackend.shared.notification;

public record NotificationErrorProps(String message, String context) { }
