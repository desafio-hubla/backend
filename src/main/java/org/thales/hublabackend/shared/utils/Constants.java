package org.thales.hublabackend.shared.utils;

public class Constants {
    public static final String NAME_STR = "NAME";
    public static final String USER_STR = "USER";
    public static final String PRODUCT_STR = "PRODUCT";
    public static final String TRANSACTION_STR = "TRANSACTION";
    public static final Integer MAX_NAME_LEN = 30;
    public static final Integer MIN_NAME_LEN = 3;

}
