package org.thales.hublabackend.shared.utils;

public class SuccessConstants {
    public static final String USER_FOUND = "USER FOUND: '{}'.";
    public static final String PRODUCT_FOUND = "USER FOUND: '{}'.";
    public static final String TRANSACTION_FOUND = "TRANSACTION FOUND: '{}'.";
    public static final String USER_INSERTED_ON_GATEWAY = "USER WITH NAME: '{}' SUCCESSFULLY INSERTED ON GATEWAY.";
    public static final String TRANSACTION_INSERTED_ON_GATEWAY = "TRANSACTION WITH ID: '{}' SUCCESSFULLY INSERTED ON GATEWAY.";
    public static final String CHANGED_BALANCE_ON_GATEWAY = "BALANCE OF RECORDS WITH TRANSACTION WITH ID: '{}' SUCCESSFULLY UṔDATED ON GATEWAY.";
    public static final String PRODUCT_INSERTED_ON_GATEWAY = "PRODUCT WITH NAME: '{}' SUCCESSFULLY INSERTED ON GATEWAY.";
    public static final String USER_VALIDATED = "OBJECT USER '{}' SUCCESSFULLY VALIDATED.";
    public static final String TRANSACTION_VALIDATED = "OBJECT TRANSACTION '{}' SUCCESSFULLY VALIDATED.";
    public static final String PRODUCT_VALIDATED = "OBJECT PRODUCT '{}' SUCCESSFULLY VALIDATED.";
    public static final String FOUND_X_PRODUCTS = "GOT {} PRODUCTS.";
    public static final String FOUND_X_TRANSACTIONS = "GOT {} TRANSACTIONS.";
}
