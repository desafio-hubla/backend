package org.thales.hublabackend.shared.utils;

public class ErrorConstants {
    public static final String STRING_SHOULD_NOT_BE_NULL = "'{}' SHOULD NOT BE NULL.";
    public static final String STRING_SHOULD_NOT_BE_BLANK = "'{}' SHOULD NOT BE BLANK.";
    public static final String NAME_LENGTH_INVALID = "'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";
    public static final String NEGATIVE_AMOUNT_OF_SELLS = "YOU CAN NOT HAVE NEGATIVE AMOUNT OF SELLS";
    public static final String NEGATIVE_PRICE_OF_PRODUCT = "YOU CAN NOT HAVE NEGATIVE PRICE OF PRODUCT";
    public static final String NEGATIVE_TRANSACTION = "YOU CAN NOT HAVE NEGATIVE TRANSACTION";
    public static final String THIS_KIND_OF_TRANSACTION_DOES_NOT_EXISTS = "THIS KIND OF TRANSACTION DOES NOT EXISTS";
    public static final String THIS_KIND_OF_USER_DOES_NOT_EXISTS = "THIS KIND OF USER DOES NOT EXISTS";
    public static final String GATEWAY_FIND_STR = "COULD NOT FIND OBJECT {}";
    public static final String USER_NOT_FOUND = "USER WITH NAME '{}' NOT FOUND.";
    public static final String PRODUCT_NOT_FOUND = "PRODUCT WITH NAME '{}' NOT FOUND.";
    public static final String TRANSACTION_NOT_FOUND = "TRANSACTION WITH ID '{}' NOT FOUND.";
    public static final String USER_ALREADY_EXISTS = "USER WITH CPF: {} ALREADY EXISTS.";
    public static final String PRODUCT_ALREADY_EXISTS = "PRODUCT WITH NAME: '{}' ALREADY EXISTS.";
    public static final String USER_VALIDATION_ERRORS = "OBJECT USER '{}' VALIDATED BUT RETURNED WITH ERRORS: {}.";
    public static final String TRANSACTION_VALIDATION_ERRORS = "OBJECT TRANSACTION '{}' VALIDATED BUT RETURNED WITH ERRORS: {}.";
    public static final String PRODUCT_VALIDATION_ERRORS = "OBJECT PRODUCT '{}' VALIDATED BUT RETURNED WITH ERRORS: {}.";
    public static final String GATEWAY_INSERT_STR = "COULD NOT INSERT OBJECT {}";
    public static final String GATEWAY_UPDATE_STR = "COULD NOT UPDATE OBJECT {}";
    public static final String GATEWAY_ERROR_STR = "GATEWAY ERROR.";


}
