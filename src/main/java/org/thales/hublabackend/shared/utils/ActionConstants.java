package org.thales.hublabackend.shared.utils;

public class ActionConstants {

    public static final String FIND_USER_BY_NAME = "FINDING USER WITH NAME: '{}'...";
    public static final String FIND_TRANSACTION_BY_PRODUCT = "FINDING TRANSACTION BY PRODUCT ID: '{}'...";
    public static final String FIND_TRANSACTION_BY_ID = "FINDING TRANSACTION BY ID: '{}'...";
    public static final String FIND_PRODUCT_BY_NAME = "FINDING PRODUCT WITH NAME: '{}'...";
    public static final String CREATE_USER = "CREATING USER: '{}'...";
    public static final String CREATE_TRANSACTION = "CREATING TRANSACTION: '{}'...";
    public static final String CREATE_PRODUCT = "CREATING PRODUCT: '{}'...";
    public static final String FIND_ALL_PRODUCTS = "GETTING ALL PRODUCTS...";

}
