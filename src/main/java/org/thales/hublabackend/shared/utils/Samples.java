package org.thales.hublabackend.shared.utils;

public class Samples {

    public static final String USER_NAME_SAMPLE = "THALES";
    public static final String PRODUCT_NAME_SAMPLE = "HUBLA FULLSTACK";
    public static final Integer PRODUCT_PRICE_SAMPLE = 13245;

}
