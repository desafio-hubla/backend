<div style="text-align: center">
  <p align="center" style>
    <img src="https://gitlab.com/desafio-hubla/backend/-/raw/main/public/logo.png" width="150"/>&nbsp;
  </p>  
  <h1 align="center">🚀 Serviço: Gerenciador de transações</h1>
  <p align="center">
    Monolito referente ao desafio hubla que abstrai todo fluxo de negócio<br/> 
    que abrange a normalização dos dados e interação com um banco de dados relacional<br/> 
    utilizando clean arch, DDD, 12 Factor App e as<br />
    boas práticas atuais do mercado. Espero que gostem!!
  </p>
</div>
<br />

## Para compreender melhor em detalhes tudo que será explicado abaixo recomendo que leia a [documentação do sistema](https://heady-swift-7de.notion.site/Hubla-d2b8cead7a6349c9b1905d3ce92cb266)

## Requisitos Funcionais

Sua aplicação deve:

- [X] Fazer o parsing do arquivo recebido, normalizar os dados e armazená-los em um banco de dados relacional, seguindo as definições de interpretação do arquivo;
- [X] Exibir a lista das transações de produtos importadas por produtor/afiliado, com um totalizador do valor das transações realizadas.

## Requisitos Não Funcionais

- [X] Escreva um README descrevendo o projeto e como fazer o setup.
- [X] A aplicação deve ser simples de configurar e rodar, compatível com ambiente Unix. Você deve utilizar apenas bibliotecas gratuitas ou livres.
- [X] Utilize docker para os diferentes serviços que compõe a aplicação para que funcione facilmente fora do seu ambiente pessoal.
- [X] Use qualquer banco de dados relacional.
- [X] Use commits pequenos no Git e escreva uma boa descrição para cada um.
- [X] Escreva unit tests tanto no backend quanto do frontend.
  - Devido a falta de complexidades de negócio no frontend acreditei não ser necessário a criação de testes unitários.  
- [X] Faça o código mais legível e limpo possível.
- [X] Escreva o código (nomes e comentários) em inglês. A documentação pode ser em português se preferir.

## Requisitos Bônus

Sua aplicação não precisa, mas ficaremos impressionados se ela:

- [X] Tiver documentação das APIs do backend.
  - OpenApi
- [ ] Utilizar docker-compose para orquestar os serviços num todo.
- [ ] Ter testes de integração ou end-to-end.
- [ ] Tiver toda a documentação escrita em inglês fácil de entender. 
- [ ] Lidar com autenticação e/ou autorização.


## Responsabilidades do Monolito

- [X] Gerenciamento de usuários
- [X] Gerenciamento de produtos
- [X] Gerenciamento de transações

## Ferramentas necessárias para o projeto localmente

- IDE de sua preferência
- Navegador de sua preferência
- JDK 17
  - Utilizei o sdkman para instalação da versão **17.0.5-amzn** com auxílio do sdkman

## Pré-execução

1. Clonar o repositório:
```sh
git clone https://gitlab.com/desafio-hubla/backend.git
```

2. Entre na pasta referente ao repositório:
```sh
cd backend
```

## Banco de dados
O banco de dados principal é um H2 e para subir localmente não precisamos de 
passos extras, a própria aplicação se encarrega de subir as tabelas descritas
no arquivo presente em ```src/main/resources/data.sql```

> É importante mencionar que pela forma que o sistema foi escrito 
> toda vez que a aplicação é derrubada nossos registros serão removidos 
> do banco

## Testes

1. Execute o comando
```shell
./gradlew test
```

> Também é possível executar via IDE. Utilizando o Intellij vá até a pasta
> **src/test/java/org/thales/hublabackend** clique com o botão direito e na metade 
> das opções apresentadas escolha **Run Tests in 'org.thales''**

### Características do sistema

- [x] Utilizado comunicação síncrona na entrada de requisições
- [x] Utilizado H2 como banco de dados

#### Pontos negativos do microserviço

1. Muitas responsabilidades gerando muita complexidade;
2. Falta de resiliência;
3. Recursos em excesso.
